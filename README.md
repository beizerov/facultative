# FACULTATIVE

##### [Task](https://gitlab.com/beizerov/facultative/blob/master/TASK.md)


##### JAVA VERSION 11

##### [HikariCP](https://github.com/brettwooldridge/HikariCP) connection pool is used to connect to the database

##### MySQL database

##### Use the following environment variables to configure DBCP

* MYSQL_DB_HOST, MYSQL_DB_NAME, MYSQL_DB_USERNAME, MYSQL_DB_PASSWORD,

* MYSQL_CP_MIN_IDLE, MYSQL_CP_MAX_POOL_SIZE

[![pipeline status](https://gitlab.com/beizerov/facultative/badges/master/pipeline.svg)](https://gitlab.com/beizerov/facultative/-/commits/master)
