package ua.nure.beizerov.facultative.persistence.dao;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import ua.nure.beizerov.facultative.configuration.DataSourceManagement;
import ua.nure.beizerov.facultative.model.Subject;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;


@TestMethodOrder(OrderAnnotation.class)
class SubjectDaoRdbmsTest {
	
	private static Connection connection;
	private static SubjectDaoRdbms subjectDaoRdbms;
	private static Subject subject;
	private static Stream<Subject> stream;
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		DataSourceManagement dataSourceManagement = new DataSourceManagement(
				RdbmsResourceNaming.MY_SQL_TEST
		);
			
		connection = dataSourceManagement.getConnection();		
		
		subjectDaoRdbms = new SubjectDaoRdbms(RdbmsResourceNaming.MY_SQL_TEST);
		
		subject = new Subject();
		
		subject.setId(1);
		subject.setName("Test");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		String sqlSetSubjectsAutoIncrementInOne = 
				"ALTER TABLE subjects AUTO_INCREMENT = 1";
		
		Statement stmt = connection.createStatement();
		
		stmt.execute(sqlSetSubjectsAutoIncrementInOne);
		
		connection.close();
	}
	
	
	@AfterEach
	final void tearDownAfterEach() {
		if (stream != null) {
			stream.close();
		}
	}
	

	@Test
	@Order(1)
	final void testSubjectDaoRdbms() {
		assertNotNull(subjectDaoRdbms, "subjectDaoRdbms can't be null");
	}

	@Test
	@Order(2)
	final void testCreate() throws SQLException {
		assertDoesNotThrow(() -> subjectDaoRdbms.create(subject));
	}

	@Test
	@Order(5)
	final void testDelete() throws SQLException {
		assertDoesNotThrow(() -> subjectDaoRdbms.delete(subject.getId()));
	}

	@Test
	@Order(3)
	final void testUpdate() throws SQLException {
		assertDoesNotThrow(() -> subjectDaoRdbms.update(subject));
	}

	@Test
	@Order(4)
	final void testGetAll() throws SQLException {
		stream = subjectDaoRdbms.getAll();
				
		assertTrue(stream.findFirst().isPresent());
	}
}
