package ua.nure.beizerov.facultative.persistence.dao;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import ua.nure.beizerov.facultative.configuration.DataSourceManagement;
import ua.nure.beizerov.facultative.exception.GradeException;
import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.model.Grade;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;
import ua.nure.beizerov.facultative.persistence.dto.CourseDto;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;


@TestMethodOrder(OrderAnnotation.class)
class CourseDaoRdbmsTest {
	
	private static Connection connection;
	private static CourseDaoRdbms courseDaoRdbms;
	private static Course course;
	private static Stream<CourseDto> stream;
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		DataSourceManagement dataSourceManagement = new DataSourceManagement(
			RdbmsResourceNaming.MY_SQL_TEST
		);
		
		connection = dataSourceManagement.getConnection();
		
		String sqlfillUsersTable = "INSERT INTO users"
				+ "(email, login, first_name, last_name, "
				+ "password, role, locale_language)"
				+ "VALUES ROW ('seo@gmail.com', 'deathstar', 'Darth', "
				+ "'Vader', 'pass12345?', 'ADMIN', 'en'),"
				+ "ROW ('seoc@gmail.com', 'one', 'Obi-Wan', "
				+ "'Kenobi', 'pass12345?', 'TEACHER', 'en'),"
				+ "ROW ('t1@gmail.com', 'three', 'Ban', "
				+ "'Keno', 'pass12345?', 'TEACHER', 'en'),"
				+ "ROW ('t2@gmail.com', 'four', 'Wan', "
				+ "'Lee', 'pass12345?', 'TEACHER', 'en'),"
				+ "ROW ('s1@gmail.com', 'five', 'Annabelle', "
				+ "'Swan', 'pass12345?', 'STUDENT', 'en'),"
				+ "ROW ('s2@gmail.com', 'six', 'Jane', "
				+ "'Swan', 'pass12345?', 'STUDENT', 'en')";
		
		String sqlfillCoursesTable = "INSERT INTO courses "
				+ "(course_name, start_date, end_date)"
				+ "VALUES ROW ('Algebra', '2020-04-15', '2020-08-25'),"
				+ "ROW ('Thermodynamics', '2020-04-15', '2020-08-25'),"
				+ "ROW ('Physiological mechanisms', '2020-04-15', '2020-07-25')";
		
		String sqlfillSubjectsTable = "INSERT INTO subjects "
				+ "(subject_name)"
				+ "VALUES ROW ('Math'),"
				+ "ROW ('Physics'),"
				+ "ROW ('Biology')";
		
		String sqlfillSubjectsCoursesTable = "INSERT INTO subjects_courses "
				+ "VALUES ROW (1, 1),"
				+ "ROW (2, 2),"
				+ "ROW (3, 3)";
		
		Statement stmt = connection.createStatement();
		
		stmt.execute(sqlfillUsersTable);
		stmt.execute(sqlfillCoursesTable);
		stmt.execute(sqlfillSubjectsTable);
		stmt.execute(sqlfillSubjectsCoursesTable);
		
		courseDaoRdbms = new CourseDaoRdbms(RdbmsResourceNaming.MY_SQL_TEST);
		
		course = new Course();
		
		course.setId(4);
		course.setName("Test Course");
		course.setStartDate(LocalDate.of(2020, 6, 15));
		course.setEndDate(LocalDate.of(2020, 10, 22));
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		String sqlSetUsersAutoIncrementInOne = 
				"ALTER TABLE users AUTO_INCREMENT = 1";
		String sqlSetSubjectsAutoIncrementInOne = 
				"ALTER TABLE subjects AUTO_INCREMENT = 1";
		String sqlSetCoursesAutoIncrementInOne = 
				"ALTER TABLE courses AUTO_INCREMENT = 1";
		String sqlDeleteAllCourses = "DELETE FROM courses";
		String sqlDeleteAllSubjects = "DELETE FROM subjects";
		String sqlDeleteAllUsers = "DELETE FROM users";
		
		Statement stmt = connection.createStatement();
		
		stmt.execute(sqlDeleteAllCourses);
		stmt.execute(sqlDeleteAllSubjects);
		stmt.execute(sqlDeleteAllUsers);
		
		stmt.execute(sqlSetUsersAutoIncrementInOne);
		stmt.execute(sqlSetSubjectsAutoIncrementInOne);
		stmt.execute(sqlSetCoursesAutoIncrementInOne);
		
		connection.close();
	}
	
	
	@AfterEach
	final void tearDownAfterEach() {
		if (stream != null) {
			stream.close();
		}
	}
	
	
	@Test
	@Order(1)
	final void testCourseDaoRdbms() {
		assertNotNull(courseDaoRdbms, "courseDaoRdbms can't be null");
	}

	@Test
	@Order(2)
	final void testCreate() throws SQLException {
		assertDoesNotThrow(() -> courseDaoRdbms.create(course));
	}

	@Test
	@Order(4)
	final void testDelete() throws SQLException {
		assertDoesNotThrow(() -> courseDaoRdbms.delete(course.getId()));
	}

	@Test
	@Order(3)
	final void testUpdate() throws SQLException {
		assertDoesNotThrow(() -> courseDaoRdbms.update(course));
	}

	@Test
	@Order(9)
	final void testGetAll() throws SQLException {
		stream = courseDaoRdbms.getAll(
			StoredProcedureParameterName.COURSE_NAME,
			ua.nure.beizerov.facultative.persistence.enumeration.Order.ASC
		);
		
		assertTrue(stream.findFirst() != null);
	}

	@Test
	@Order(10)
	final void testGetAllBySubjectId() throws SQLException {
		stream = courseDaoRdbms.getAllBySubjectId(
				1, 
				StoredProcedureParameterName.COURSE_NAME, 
				ua.nure.beizerov.facultative.persistence.enumeration.Order.ASC
		);
		
		assertTrue(stream.findFirst() != null);
	}

	@Test
	@Order(11)
	final void testGetAllByTeacherId() throws SQLException {
		stream = courseDaoRdbms.getAllByTeacherId(
				2, 
				StoredProcedureParameterName.COURSE_NAME, 
				ua.nure.beizerov.facultative.persistence.enumeration.Order.ASC
		);
		
		assertTrue(stream.findFirst() != null);
	}

	@Test
	@Order(12)
	final void testGetAllByStudentId() throws SQLException {
		stream = courseDaoRdbms.getAllByStudentId(5);
		
		assertTrue(stream.findFirst() != null);
	}

	@Test
	@Order(5)
	final void testEnrollStudent() throws SQLException {
		assertDoesNotThrow(() -> courseDaoRdbms.enrollStudent(2, 2));
	}

	@Test
	@Order(6)
	final void testAssignTeacher() throws SQLException {
		assertDoesNotThrow(() -> courseDaoRdbms.assignTeacher(2, 2));
	}

	@Test
	@Order(7)
	final void testRateStudent() throws SQLException, GradeException {
		assertDoesNotThrow(() -> courseDaoRdbms.rateStudent(
			1, 5, Grade.convertLetterGradeToGrade("A+")
		));
	}

	@Test
	@Order(8)
	final void testEditStudentGrade() throws SQLException, GradeException {
		assertDoesNotThrow(() -> courseDaoRdbms.editStudentGrade(
			1, 5, Grade.convertLetterGradeToGrade("B+")
		));
	}
}
