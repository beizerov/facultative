package ua.nure.beizerov.facultative.persistence.dao;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import ua.nure.beizerov.facultative.configuration.DataSourceManagement;
import ua.nure.beizerov.facultative.model.Role;
import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;


@TestMethodOrder(OrderAnnotation.class)
class UserDaoRdbmsTest {
	
	private static Connection connection;
	private static UserDaoRdbms userDaoRdbms;
	private static User user;
	private static Stream<User> stream;
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		DataSourceManagement dataSourceManagement = new DataSourceManagement(
				RdbmsResourceNaming.MY_SQL_TEST
		);
			
		connection = dataSourceManagement.getConnection();
		
		userDaoRdbms = new UserDaoRdbms(RdbmsResourceNaming.MY_SQL_TEST);
		
		user = new User();
		
		user.setId(1);
		user.setActive(1);
		user.setEmail("teil.comst@ema");
		user.setLogin("login");
		user.setFirstName("Name");
		user.setLastName("LastName");
		user.setPassword("password");
		user.setRole(Role.STUDENT);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		String sqlSetUsersAutoIncrementInOne = 
				"ALTER TABLE users AUTO_INCREMENT = 1";
		
		Statement stmt = connection.createStatement();
		
		stmt.execute(sqlSetUsersAutoIncrementInOne);
		
		connection.close();
	}
	
	
	@AfterEach
	final void tearDownAfterEach() {
		if (stream != null) {
			stream.close();
		}
	}
	
	
	@Test
	@Order(1)
	final void testUserDaoRdbms() {
		assertNotNull(userDaoRdbms, "userDaoRdbms can't be null");
	}

	@Test
	@Order(2)
	final void testCreate() throws SQLException {
		assertDoesNotThrow(() -> userDaoRdbms.create(user));
	}

	@Test
	@Order(3)
	final void testGetById() throws SQLException {
		assertTrue(userDaoRdbms.getById(1).isPresent());
	}

	@Test
	@Order(4)
	final void testSetActive() throws SQLException {
		assertDoesNotThrow(() -> userDaoRdbms.setActive(1, 0));
	}
	
	@Test
	@Order(5)
	final void testGetUsersByRole() throws SQLException {
		stream = userDaoRdbms.getUsersByRole(Role.STUDENT);
		
		assertTrue(stream.findFirst().isPresent());
	}
	
	@Test
	@Order(6)
	final void testGetUserByLogin() throws SQLException {
		assertTrue(userDaoRdbms.getUserByLogin("login").isPresent());
	}
	
	@Test
	@Order(7)
	final void testGetUserByEmail() throws SQLException {
		assertTrue(userDaoRdbms.getUserByEmail("teil.comst@ema").isPresent());
	}
	
	@Test
	@Order(8)
	final void testDelete() throws SQLException {
		assertDoesNotThrow(() -> userDaoRdbms.delete(user.getId()));
	}
}
