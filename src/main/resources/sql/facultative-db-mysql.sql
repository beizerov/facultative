DROP DATABASE IF EXISTS `facultative_db`;


CREATE DATABASE `facultative_db`
CHARACTER SET = utf8mb4 
COLLATE = utf8mb4_unicode_ci;

USE `facultative_db`;


CREATE TABLE `users` (
	`user_id` INT UNSIGNED AUTO_INCREMENT,
	`active` BOOL DEFAULT 0,
	`email` VARCHAR(50) CHARACTER SET ascii BINARY UNIQUE NOT NULL,
	`login` VARCHAR(25) CHARACTER SET ascii BINARY UNIQUE NOT NULL,
	`first_name` VARCHAR(25) NOT NULL,
	`last_name` VARCHAR(25) NOT NULL,
	`password` VARCHAR(50) BINARY NOT NULL,
	`role` VARCHAR(7) NOT NULL,
	`locale_language` CHAR(2),
	`path_to_photo` VARCHAR(100),
	
	PRIMARY KEY (`user_id`),
	CONSTRAINT c1_role_admin_teacher_student CHECK (
		`role` IN ('ADMIN', 'TEACHER', 'STUDENT')
	)
);

CREATE TABLE `subjects` (
	`subject_id` INT UNSIGNED AUTO_INCREMENT,
	`subject_name` VARCHAR(50) UNIQUE NOT NULL,
	`description` VARCHAR(500),
	
	PRIMARY KEY (`subject_id`)
);

CREATE TABLE `courses` (
	`course_id` INT UNSIGNED AUTO_INCREMENT,
	`course_name` VARCHAR(50) UNIQUE NOT NULL,
	`start_date` DATE NOT NULL,
	`end_date` DATE NOT NULL,
	`description` VARCHAR(500),
	
	PRIMARY KEY (`course_id`),
	CONSTRAINT c1_course_start_date_less_then_end_date CHECK(
		DATEDIFF(`courses`.`end_date`, `courses`.`start_date`) > 0
	)
);

CREATE TABLE `subjects_courses` (
	`subject_id` INT UNSIGNED NOT NULL,
	`course_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`subject_id`,`course_id`),
	CONSTRAINT f1_key_subject_courses FOREIGN KEY (`subject_id`)  REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
	CONSTRAINT f2_key_subject_courses FOREIGN KEY (`course_id`)  REFERENCES `courses` (`course_id`) ON DELETE CASCADE
);

CREATE TABLE `teachers_courses` (
	`teacher_id` INT UNSIGNED NOT NULL,
	`course_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`teacher_id`,`course_id`),
	CONSTRAINT f1_key_teachers_courses FOREIGN KEY (`teacher_id`)  REFERENCES `users` (`user_id`) ON DELETE CASCADE,
	CONSTRAINT f2_key_teachers_courses FOREIGN KEY (`course_id`)  REFERENCES `courses` (`course_id`) ON DELETE CASCADE
);

CREATE TABLE `students_courses` (
	`student_id` INT UNSIGNED NOT NULL,
	`course_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`student_id`,`course_id`),
	CONSTRAINT f1_key_students_courses FOREIGN KEY (`student_id`)  REFERENCES `users` (`user_id`) ON DELETE CASCADE,
	CONSTRAINT f2_key_students_courses FOREIGN KEY (`course_id`)  REFERENCES `courses` (`course_id`) ON DELETE CASCADE
);

/*
		------------------
		Percent	 |	Grade
		------------------
		90%-100% |	A+
		------------------
		93%-96%	 |	A
		------------------
		90%-92%	 |	A-
		------------------
		87%-89%	 |	B+
		------------------
		83%-86%	 |	B
		------------------
		80%-82%	 |	B-
		------------------
		77%-79%	 |	C+
		------------------
		73%-76%	 |	C
		------------------
		70%-72%	 |	C-
		------------------
		67%-69%	 |	D+
		------------------
		63%-66%	 |	D
		------------------
		60%-62%	 |	D-
		------------------
		0%-59%	 |	F
		------------------
*/

CREATE TABLE `gradebook` (
	`course_id` INT UNSIGNED NOT NULL,
	`student_id` INT UNSIGNED NOT NULL,
	`grade` VARCHAR(2) NOT NULL,
	
	PRIMARY KEY (`course_id`, `student_id`),
	CONSTRAINT f1_key_gradebook FOREIGN KEY (`student_id`)  REFERENCES `users` (`user_id`) ON DELETE CASCADE,
	CONSTRAINT f2_key_gradebook FOREIGN KEY (`course_id`)  REFERENCES `courses` (`course_id`) ON DELETE CASCADE
);


CREATE VIEW `view_courses` AS SELECT `c`.`course_id`, `c`.`course_name`, `c`.`start_date`, `c`.`end_date`, 
	DATEDIFF(`c`.`end_date`, `c`.`start_date`) AS `duration`,
	COUNT(`sc`.`student_id`) AS `number_of_students`, `c`.`description`, `s`.`subject_id`,
	`s`.`subject_name`,  `u`.`user_id` AS `teacher_id`, CONCAT(`u`.`first_name`, ' ', `u`.`last_name`) AS `teacher_full_name` 
FROM `courses` `c`
LEFT JOIN `students_courses` `sc` 
ON `c`.`course_id` = `sc`.`course_id`
JOIN `subjects_courses` `sub_c`
ON `sub_c`.`course_id` = `c`.`course_id`
JOIN `subjects` `s`
ON `s`.`subject_id` = `sub_c`.`subject_id`
JOIN `teachers_courses` `tc`
ON `tc`.`course_id` = `c`.`course_id`
JOIN `users` `u`
ON `u`.`user_id` = `tc`.`teacher_id`
GROUP BY `c`.`course_id`, `s`.`subject_name`, `teacher_full_name`, `u`.`first_name`, `u`.`last_name`;


DELIMITER //


CREATE PROCEDURE `get_user_by_id`(IN `user_id` INT UNSIGNED)
BEGIN
	SELECT * 
	FROM `users` 
	WHERE `users`.`user_id` = `user_id`;
END //

CREATE PROCEDURE `get_user_by_login`(IN `login` VARCHAR(25))
BEGIN
	SELECT * 
	FROM `users` 
	WHERE `users`.`login` = `login`;
END //

CREATE PROCEDURE `get_user_by_email`(IN `email` VARCHAR(50))
BEGIN
	SELECT * 
	FROM `users` 
	WHERE `users`.`email` = `email`;
END //

CREATE PROCEDURE `get_users_by_role`(IN `role` VARCHAR(7))
BEGIN
	SELECT * 
	FROM `users`
	WHERE `users`.`role` = `role`;
END //

CREATE PROCEDURE `save_user`(
	IN `active` BOOL, 
	IN `email` VARCHAR(50), 
	IN `login` VARCHAR(25), 
	IN `first_name` VARCHAR(25), 
	IN `last_name` VARCHAR(25), 
	IN `password` VARCHAR(50), 
	IN `role` VARCHAR(7), 
	IN `locale_language` CHAR(2), 
	IN `path_to_photo` VARCHAR(100)
)
BEGIN
	INSERT INTO `users` (
		`users`.`active`, `users`.`email`, `users`.`login`, `users`.`first_name`, `users`.`last_name`, 
		`users`.`password`, `users`.`role`, `users`.`locale_language`, `users`.`path_to_photo`
	)
	VALUES (
		`active`, `email`, `login`, `first_name`, `last_name`, 
		`password`, `role`, `locale_language`, `path_to_photo`
	);
END //

CREATE PROCEDURE `save_teacher_for_selected_course`(
	IN `active` BOOL, 
	IN `email` VARCHAR(50), 
	IN `login` VARCHAR(25), 
	IN `first_name` VARCHAR(25), 
	IN `last_name` VARCHAR(25), 
	IN `password` VARCHAR(50), 
	IN `role` VARCHAR(7), 
	IN `locale_language` CHAR(2), 
	IN `path_to_photo` VARCHAR(100),
	IN `course_id` INT UNSIGNED,
	OUT `isOk` INT
)
BEGIN
	DECLARE `userId` INT UNSIGNED;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
	BEGIN
		SET `isOk` = 0;
		ROLLBACK;
	END;
	
	START TRANSACTION;
		CALL `save_user`(
			`active`, `email`, `login`, `first_name`, `last_name`,
			`password`, `role`, `locale_language`, `path_to_photo`
		); 
		
		SELECT `user_id` INTO `userId`
		FROM `users`
		WHERE `users`.`login` = `login`;
		
		CALL `assign_teacher`(`userId`, `course_id`);
	COMMIT;
	
	SET `isOk` = 1;
END //


CREATE PROCEDURE `delete_user_by_id`(
		IN `user_id` INT UNSIGNED
)
BEGIN
	DELETE FROM `users` WHERE `users`.`user_id` = `user_id`; 
END //

CREATE PROCEDURE `set_user_active`(
	IN `user_id` INT UNSIGNED, IN `active` BOOL)
BEGIN
	UPDATE `users`
	SET `users`.`active` = `active`
	WHERE `users`.`user_id` = `user_id`;
END //

CREATE PROCEDURE `save_course`(
		IN `course_name` VARCHAR(50), IN `start_date` DATE ,
		IN `end_date` DATE , IN `description` VARCHAR(500)
)
BEGIN
	INSERT INTO `courses` (
		`courses`.`course_name`, `courses`.`start_date`,
		`courses`.`end_date`, `courses`.`description` 
	)
	VALUES (
		`course_name`, `start_date`, `end_date`, `description`
	);
END //

CREATE PROCEDURE `save_course_for_selected_subject`(
	IN `course_name` VARCHAR(50), 
	IN `start_date` DATE ,
	IN `end_date` DATE , 
	IN `description` VARCHAR(500), 
	IN `subject_id` INT UNSIGNED,
	OUT `isOk` INT
)
BEGIN
	DECLARE `courseId` INT UNSIGNED;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
	BEGIN
		SET `isOk` = 0;
		ROLLBACK;
	END;
	
	START TRANSACTION;
		CALL `save_course`(`course_name`, `start_date`, `end_date`, `description`); 
		
		SELECT `course_id` INTO `courseId`
		FROM `courses`
		WHERE `courses`.`course_name` = `course_name`;
		
		CALL `add_course_subject`(`subject_id`, `courseId`);
	COMMIT;
	
	SET `isOk` = 1;
END //

CREATE PROCEDURE `delete_course_by_id`(
	IN `course_id` INT UNSIGNED
)
BEGIN
	DELETE FROM `courses` WHERE `courses`.`course_id` = `course_id`; 
END //

CREATE PROCEDURE `get_courses`()
BEGIN
	SELECT * 
	FROM `courses`;
END //

CREATE PROCEDURE `get_course_by_name`(IN `course_name` VARCHAR(50))
BEGIN
	SELECT * 
	FROM `courses`
	WHERE `courses`.`course_name` = `course_name`;
END //

CREATE PROCEDURE `get_courses_sorted_by_column_name_and_order_by`(
	IN `column_name` VARCHAR(20),  
	IN `sort_order` VARCHAR(4)
)
BEGIN
	SET @sql = CONCAT(
		'SELECT * ',
		'FROM view_courses ',
		'ORDER BY ', `column_name`, ' ', `sort_order`
	);

	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END //

CREATE PROCEDURE `get_courses_by_subject_id_sorted_by_column_name_and_order_by`(
	IN `subject_id` INT UNSIGNED,
	IN `column_name` VARCHAR(20),  
	IN `sort_order` VARCHAR(4)
)
BEGIN
	SET @param1 = `subject_id`;

	SET @sql = CONCAT('SELECT * FROM view_courses vc WHERE vc.`subject_id` = ? ORDER BY ',
		`column_name`, ' ', `sort_order`
	);

	PREPARE stmt FROM @sql;
	EXECUTE stmt USING @param1;
	DEALLOCATE PREPARE stmt;
END //

CREATE PROCEDURE `get_courses_by_teacher_id_sorted_by_col_name_and_order_by`(
	IN `teacher_id` INT UNSIGNED,
	IN `column_name` VARCHAR(20),  
	IN `sort_order` VARCHAR(4)
)
BEGIN
	SET @param1 = `teacher_id`;

	SET @sql = CONCAT('SELECT * FROM view_courses vc WHERE vc.teacher_id = ? ORDER BY ',
		`column_name`, ' ', `sort_order`
	);

	PREPARE stmt FROM @sql;
	EXECUTE stmt USING @param1;
	DEALLOCATE PREPARE stmt;
END //

CREATE PROCEDURE `get_student_courses_by_student_id`(IN `student_id` INT UNSIGNED)
BEGIN
	SELECT 
		`vc`.`course_id`, `vc`.`course_name`, `vc`.`start_date`,
		`vc`.`end_date`, `vc`.`duration`, `vc`.`number_of_students`,
		`vc`.`description`, `vc`.`subject_name`, `vc`.`teacher_full_name`,
		CASE
			WHEN DATE(`vc`.`start_date`) > CURRENT_DATE() THEN 'NOT STARTED'
			WHEN DATE(`vc`.`end_date`) < CURRENT_DATE() THEN 'FINISHED'
			ELSE 'IN PROGRESS'
		END `progress`
	FROM `view_courses` `vc`, `students_courses` `sc`
	WHERE `vc`.`course_id` = `sc`.`course_id` AND `sc`.`student_id` = `student_id`;
END //

CREATE PROCEDURE `assign_teacher`(IN `teacher_id` INT UNSIGNED, IN `course_id` INT UNSIGNED)
BEGIN
	INSERT INTO `teachers_courses`
	VALUES (`teacher_id`, `course_id`);
END //

CREATE PROCEDURE `enroll_student`(IN `student_id` INT UNSIGNED, IN `course_id` INT UNSIGNED)
BEGIN
	INSERT INTO `students_courses`
	VALUES (`student_id`, `course_id`);
END //

CREATE PROCEDURE `rate_student`(
	IN `course_id` INT UNSIGNED,
	IN `student_id` INT UNSIGNED,
	IN `grade` VARCHAR(2)
)
BEGIN
	INSERT INTO `gradebook`
	VALUES (`course_id`, `student_id`, `grade`);
END //

CREATE PROCEDURE `update_student_grade`(
	IN `course_id` INT UNSIGNED,
	IN `student_id` INT UNSIGNED,
	IN `grade` VARCHAR(2)
)
BEGIN
	UPDATE `gradebook`
	SET `gradebook`.`grade` = `grade` 
	WHERE `gradebook`.`course_id` = `course_id` AND `gradebook`.`student_id` = `student_id`;
END //

CREATE PROCEDURE `update_course`(
	IN `course_id` INT UNSIGNED,
	IN `course_name` VARCHAR(50),
	IN `start_date` DATE,
	IN `end_date` DATE,
	IN `description` VARCHAR(500)
)
BEGIN
	UPDATE `courses`
	SET `courses`.`course_name` = `course_name`, `courses`.`start_date` = `start_date`,
		`courses`.`end_date` = `end_date`, `courses`.`description` = `description`
	WHERE `courses`.`course_id` = `course_id`;
END //

CREATE PROCEDURE `add_course_subject`(
	IN `subject_id` INT UNSIGNED, IN `course_id` INT UNSIGNED
)
BEGIN
	INSERT INTO `subjects_courses` (`subjects_courses`.`subject_id`, `subjects_courses`.`course_id`)
	VALUES (`subject_id`, `course_id`);
END //

CREATE PROCEDURE `save_subject`(
		IN `subject_name` VARCHAR(50), IN `description` VARCHAR(500)
)
BEGIN
	INSERT INTO `subjects` (
		`subjects`.`subject_name`, `subjects`.`description`
	)
	VALUES (`subject_name`, `description`);
END //

CREATE PROCEDURE `delete_subject_by_id`(
		IN `subject_id` INT UNSIGNED
)
BEGIN
	DELETE FROM `subjects` WHERE `subjects`.`subject_id` = `subject_id`; 
END //

CREATE PROCEDURE `update_subject`(
	IN `subject_id` INT UNSIGNED,
	IN `subject_name` VARCHAR(50),
	IN `description` VARCHAR(500)
)
BEGIN
	UPDATE `subjects`
	SET `subjects`.`subject_name` = `subject_name`, `subjects`.`description` = `description`
	WHERE `subjects`.`subject_id` = `subject_id`;
END //

CREATE PROCEDURE `get_subjects`()
BEGIN
	SELECT *
	FROM `subjects`;
END //


DELIMITER ;


/*	
 * The default administrator account, 
 * after the first start it is recommended to change the data and password.
 */
INSERT INTO `users` (
	`active`, `email`, `login`, `first_name`, `last_name`, 
	`password`, `role`, `locale_language`
)
VALUES (
	1, 'adminus@email.com', 'adminus', 'Darth', 
	'Vader', 'adm!n123', 'ADMIN', 'en'
);
