<%@ include file="/WEB-INF/fragment/directive/page.jspf" %>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf" %>

<c:set var="title" value="Login" />
<%@ include file="/WEB-INF/fragment/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">

<body>
	<div class="wrapper fadeInDown">
	  <div id="formContent">
	    <!-- Tabs Titles -->
	
	
		<!-- Icon -->
	    <div class="fadeIn first">
	      <img src="${pageContext.request.contextPath}/image/user-300x230.png" id="icon" alt="User Icon" />
	    </div>
	
	    <!-- Login Form -->
	    <form action="login" method="POST">
	      <input type="text" id="login" class="fadeIn second" name="login" 
	      placeholder="login" 
	      pattern="^[\p{L}\p{Nd}]{5,}\.?(?=[\p{L}\p{Nd}])[\p{L}\p{Nd}]+$"
	      title="Minimum 6 characters. Letters, numbers and period. A period character cannot be first or last."
	      required>
	      <input type="password" id="password" class="fadeIn third" name="password"
	      pattern=".{8,}"
	      placeholder="password" required
	      title="Minimum 8 characters."
	      required>
	      <input type="submit" class="fadeIn fourth" value= "LOG IN" <%-- '<fmt:message key="login_btn"/>' --%>>
	    </form>
	
	    <!-- registration.jsp -->
	    <div id="formFooter">
			<a class="underlineHover" href="registration">
					<%-- <fmt:message key="registration_btn"/> --%>
					Registration
			</a>
		</div>
	  </div>
	</div>
</body>
</html>