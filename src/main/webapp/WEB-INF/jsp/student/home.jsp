<%@ include file="/WEB-INF/fragment/directive/page.jspf"%>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf"%>

<c:set var="title" value="Home" />
<%@ include file="/WEB-INF/fragment/head.jspf"%>

<body>
	<nav class="panel">
		<%@ include file="/WEB-INF/fragment/logout.jspf"%>
	</nav>

	<h1>Displaying Courses List</h1>

	<table bgcolor="FFFF7F" border="1" width="500" align="center">
		<tr bgcolor="00FF7F">
			<th><b>Course</b></th>
			<th><b>Start date</b></th>
			<th><b>End date</b></th>
			<th><b>Duration</b></th>
			<th><b>Number of students</b></th>
			<th><b>Description</b></th>
			<th><b>Subject</b></th>
			<th><b>Teacher</b></th>
		</tr>
		
		<form id="form3" method="post" action="${sessionScope.user.login}">
			<c:forEach var="course" items="${requestScope.courses}">
				<myTagLib:printCourse course="${course}" />
			</c:forEach>
		</form>
	</table>
</body>
</html>