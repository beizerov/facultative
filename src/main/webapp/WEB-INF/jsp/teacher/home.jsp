<%@ include file="/WEB-INF/fragment/directive/page.jspf" %>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf" %>

<c:set var="title" value="Home" />
<%@ include file="/WEB-INF/fragment/head.jspf" %>

<body>
	<nav class="panel">
		<%@ include file="/WEB-INF/fragment/logout.jspf" %>
	</nav>

	<h1>TEACHER'S HOME</h1>
</body>
</html>