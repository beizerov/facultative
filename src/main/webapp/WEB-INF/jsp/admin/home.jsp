<%@ include file="/WEB-INF/fragment/directive/page.jspf" %>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf" %>

<c:set var="title" value="Home" />
<%@ include file="/WEB-INF/fragment/head.jspf" %>

<body>
	<nav class="panel">
		<%@ include file="/WEB-INF/fragment/admin/registerSubject.jspf" %>
		<%@ include file="/WEB-INF/fragment/admin/registerCourse.jspf" %>
		<%@ include file="/WEB-INF/fragment/admin/registerTeacher.jspf" %>
		<%@ include file="/WEB-INF/fragment/logout.jspf" %>
	</nav>
	
	<div class="container">
	
		<h1>ADMIN'S HOME</h1>
	
		<c:set var="user" value="${requestScope['user']}" />

        <p><c:out value = "${user.email}"/></p>
    </div>
</body>
</html>