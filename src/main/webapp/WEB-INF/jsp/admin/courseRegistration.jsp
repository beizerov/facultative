<%@ include file="/WEB-INF/fragment/directive/page.jspf"%>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf"%>

<c:set var="title" value="Course registration" />
<%@ include file="/WEB-INF/fragment/head.jspf"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/registration.css">

<body>
	<nav class="panel">
		<c:set var="role" value="admin" />

		<%@ include file="/WEB-INF/fragment/home.jspf"%>
		<%@ include file="/WEB-INF/fragment/admin/registerSubject.jspf" %>
		<%@ include file="/WEB-INF/fragment/admin/registerTeacher.jspf"%>
		<%@ include file="/WEB-INF/fragment/logout.jspf"%>
	</nav>
	
	<div class="container">
		<form id="courseRegistrationForm" class="form-horizontal" role="form"
			method="post" action="courseRegistration">
	
			<script type="text/javascript">
				setTimeout(function() {
					$("#successAddedCourse").hide();
				}, 1500);
				
			</script>
	
			<c:if test="${not empty successMessage}">
				<span id="successAddedCourse" class="success">${successMessage}</span>
			</c:if>
			
			<h2>Course Registration</h2>
			<div class="form-group">
				<label for="courseName" class="col-sm-3 control-label">
					Name
				</label>
				<div class="col-sm-9">
					<input type="text" id="courseName" name="courseName"
						value="${param.courseName}" 
						placeholder="Course name"
						class="form-control" 
						autofocus
						pattern="^\w{3,40}$"
						title="Only letters, digits and underscore characters. Minimum 3 letters. Maximum 40 letters. Example: DDD"
						required>
					<span class="error">${messages.courseNameError}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="startDate" class="col-sm-3 control-label"> Start
					date </label>
				<div class="col-sm-9">
					<input type="date" id="startDate" name="startDate"
						value="${param.startDate}"
						placeholder="Start date" class="form-control" autofocus
						title="Start date" required>
				</div>
			</div>
			<div class="form-group">
				<label for="endDate" class="col-sm-3 control-label"> End
					date </label>
				<div class="col-sm-9">
					<input type="date" id="endDate" name="endDate"
						value="${param.endDate}"
						placeholder="End date" class="form-control" autofocus
						title="End date" required>
				</div>
			</div>
			<div class="form-group">
				<label for="courseDescription" class="col-sm-3 control-label">
					Description </label>
				<div class="col-sm-9">
					<textarea id="courseDescription" name="courseDescription"
						placeholder="Description" rows="5" cols="80" class="form-control"
						autofocus title="Description" required>
					</textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="subjectSelection" class="col-sm-3 control-label">
					Subject
				</label>
				<c:if test="${empty subjects}">
					<span class="error">First, register a subject</span>
				</c:if>
			
				<c:if test="${not empty subjects}">
					<select id="subjectSelection" name="subjectSelection">
						<c:forEach var="subject" items="${subjects}">
							<option <c:if test="${subject.id == param.subjectSelection}">selected</c:if> value="${subject.id}">${subject.name}</option>
						</c:forEach>
					</select>
			
					<button type="submit" class="btn btn-primary btn-block">Register</button>
				</c:if>
			</div>
		</form>
		<!-- /form -->
	</div>
	<!-- ./container -->
</body>
</html>