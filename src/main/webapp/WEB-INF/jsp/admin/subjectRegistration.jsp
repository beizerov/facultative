<%@ include file="/WEB-INF/fragment/directive/page.jspf"%>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf"%>

<c:set var="title" value="Subject registration" />
<%@ include file="/WEB-INF/fragment/head.jspf"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/registration.css">

<body>
	<nav class="panel">
		<c:set var="role" value="admin" />

		<%@ include file="/WEB-INF/fragment/home.jspf"%>
		<%@ include file="/WEB-INF/fragment/admin/registerCourse.jspf" %>
		<%@ include file="/WEB-INF/fragment/admin/registerTeacher.jspf"%>
		<%@ include file="/WEB-INF/fragment/logout.jspf"%>
	</nav>
	
	<div class="container">
		<form id="subjectRegistrationForm" class="form-horizontal" role="form"
			method="post" action="subjectRegistration">
	
			<script type="text/javascript">
				setTimeout(function() {
					$("#successAddedSubject").hide();
				}, 1500);
				
			</script>
	
			<c:if test="${not empty successMessage}">
				<span id="successAddedSubject" class="success">${successMessage}</span>
			</c:if>
			
			<h2>Subject Registration</h2>
			<div class="form-group">
				<label for="subjectName" class="col-sm-3 control-label">
					Name
				</label>
				<div class="col-sm-9">
					<input type="text" id="subjectName" name="subjectName"
						value="${param.subjectName}" 
						placeholder="Subject name"
						class="form-control" 
						autofocus
						pattern="^\w{3,40}$"
						title="Only letters, digits and underscore characters. Minimum 3 letters. Maximum 40 letters. Example: DDD"
						required>
					<span class="error">${messages.courseNameError}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="subjectDescription" class="col-sm-3 control-label">
					Description </label>
				<div class="col-sm-9">
					<textarea id="subjectDescription" name="subjectDescription"
						placeholder="Description" rows="5" cols="80" class="form-control"
						autofocus title="Description" required>
					</textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-block">Register</button>
		</form>
		<!-- /form -->
	</div>
	<!-- ./container -->
</body>
</html>