<%@ include file="/WEB-INF/fragment/directive/page.jspf"%>
<%@ include file="/WEB-INF/fragment/directive/taglib.jspf"%>

<c:set var="title" value="Registration" />
<%@ include file="/WEB-INF/fragment/head.jspf"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/registration.css">

<body>
	<div class="container">
		<form id="studentRegistrationForm" 
			class="form-horizontal" 
			role="form" 
			method="post" 
			action="registration"
			enctype="multipart/form-data">
			
			
			<h2>Registration</h2>
			<div class="form-group">
				<label for="email" class="col-sm-3 control-label">Email</label>
				<div class="col-sm-9">
					<input type="email" id="email" name="email" 
						value="${param.email}"
					 	placeholder="email@mail.com"
						class="form-control"
						pattern="^(?:[\p{L}\p{Nd}](\.(?=[\p{L}\p{Nd}]))?[\p{L}\p{Nd}]?){6,}@[a-z]+\.[a-z]{2,}$"
						title="Sorry, only letters [A-Za-z], numbers [0-9], and periods[.] are allowed. The period character cannot be at the beginning or at the end. In addition, there cannot be a period character after period character."
						required>
						<span class="error">${messages.emailError}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="login" class="col-sm-3 control-label">Login</label>
				<div class="col-sm-9">
					<input type="text" id="login" name="login"
						value="${param.login}"
					 	placeholder="Login" 
					 	pattern="^(?:[\p{L}\p{Nd}]\.?(?=[\p{L}\p{Nd}])[\p{L}\p{Nd}]{0,1}){5,24}$"
					 	title="Sorry, your login must be between 6 and 30 characters long. Letters, numbers and period. A period character cannot be first or last."
					 	class="form-control" autofocus required>
					 <span class="error">${messages.loginError}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="fname" class="col-sm-3 control-label">First
					Name</label>
				<div class="col-sm-9">
					<input type="text" id="firstName" name="firstName"
					 placeholder="First Name"
						class="form-control" autofocus
						value="${param.firstName}"
						pattern="^\p{Lu}\p{Ll}{2,12}$"
						title="The first letter is uppercase. Minimum 3 letters. Example: Sam"
						required>
						<span class="error">${messages.firstNameError}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="lname" class="col-sm-3 control-label">Last
					Name</label>
				<div class="col-sm-9">
					<input type="text" id="lastName" name="lastName"
					 placeholder="Last Name"
						class="form-control" autofocus
						value="${param.lastName}"
						pattern="^\p{Lu}\p{Ll}{2,12}$"
						title="The first letter is uppercase. Minimum 3 letters. Example: Sam"
						required>
						<span class="error">${messages.lastNameError}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-3 control-label">Password</label>
				<div class="col-sm-9">
					<input type="password" id="password" name="password"
					 	value="${param.password}"
					 	placeholder="Password"
						class="form-control" pattern=".{8,}" title="Minimum 8 characters"
						required>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9">
				<label for="file-name" class="control-label">
					<img id="student_avatar" alt="Avatar" src="${pageContext.request.contextPath}/image/user-300x230.png">
				</label>
				<input type="file" id="fileName" name="fileName" />
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-block">Register</button>
		</form>
		<!-- /form -->
		
		<a
			id="ret_to_login" 
			href="${applicationScope['contextPath']}" 
			class="btn btn-block">Return to login form
		</a>
	</div>
	<!-- ./container -->
</body>
</html>