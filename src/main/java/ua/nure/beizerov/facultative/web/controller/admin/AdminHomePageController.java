package ua.nure.beizerov.facultative.web.controller.admin;


import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.service.AuthenticationService;


/**
 * Servlet implementation class AdminController
 */
@WebServlet("/admin/*")
public class AdminHomePageController extends HttpServlet {

	private static final long serialVersionUID = 1860239447174091340L;
	
	private static final Logger LOGGER = Logger.getLogger(AdminHomePageController.class);

	
	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
		
		try {
			Optional<HttpSession> session = Optional.ofNullable(
					httpServletRequest.getSession(false)
			);
			
			session.ifPresent(currentSession -> 
				AuthenticationService.getAuthenticatedUser(currentSession)
					.ifPresent(authUser ->
						httpServletRequest.setAttribute("user", authUser)
				)
			);
			
			httpServletRequest
				.getRequestDispatcher("/WEB-INF/jsp/admin/home.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doPost");
	}
}
