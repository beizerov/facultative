package ua.nure.beizerov.facultative.web.controller.student;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.persistence.dao.UserDaoRdbms;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.service.UserRegistrationService;
import ua.nure.beizerov.facultative.web.util.Util;


/**
 * Servlet implementation class StudentRegistrationController
 */
@WebServlet("/registration")
@MultipartConfig(
		maxFileSize = 1049000 // Max file size 1 mebibit
)
public class StudentRegistrationPageController extends HttpServlet {
	
	private static final long serialVersionUID = 888352558509679574L;
	
	private static final Logger LOGGER = Logger.getLogger(StudentRegistrationPageController.class);
	
	
	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
		
		try {
			 httpServletRequest
				.getRequestDispatcher("/registration.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) 
	throws ServletException, IOException {
		LOGGER.debug("doPost");
		
		UserRegistrationService userRegistrationService = new UserRegistrationService(
				new UserDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		try {
			if (userRegistrationService.registerStudent(httpServletRequest)) {
				Util.redirectToContextPath(
					httpServletRequest,
					httpServletResponse
				);
			} else {
				doGet(httpServletRequest, httpServletResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
}
