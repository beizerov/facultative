package ua.nure.beizerov.facultative.web.controller.teacher;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class TeacherController
 */
@WebServlet("/teacher/*")
public class TeacherHomePageController extends HttpServlet {

	private static final long serialVersionUID = 1860239447174091340L;
	
	private static final Logger LOGGER = Logger.getLogger(TeacherHomePageController.class);

	
	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
		
		try {
			httpServletRequest
				.getRequestDispatcher("/WEB-INF/jsp/teacher/home.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest, 
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doPost");
	}
}
