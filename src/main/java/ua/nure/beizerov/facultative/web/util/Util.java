package ua.nure.beizerov.facultative.web.util;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public final class Util {

	private Util() {
		throw new IllegalStateException("Util class");
	}
	
	
	public static void redirectToContextPath(
			HttpServletRequest httpRequest, HttpServletResponse httpResponse
	) throws IOException {
		httpResponse.sendRedirect(
			httpRequest
				.getServletContext()
				.getAttribute("contextPath")
				.toString()
		);
	}
}
