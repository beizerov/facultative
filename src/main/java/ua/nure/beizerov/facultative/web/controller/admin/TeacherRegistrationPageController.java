package ua.nure.beizerov.facultative.web.controller.admin;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.persistence.dao.CourseDaoRdbms;
import ua.nure.beizerov.facultative.persistence.dao.UserDaoRdbms;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.service.GettingCourseService;
import ua.nure.beizerov.facultative.service.UserRegistrationService;
import ua.nure.beizerov.facultative.service.util.MessageService;
import ua.nure.beizerov.facultative.web.constant.Constant;


/**
 * Servlet implementation class TeacherRegistrationPageController
 */
@WebServlet("/admin/teacherRegistration")
@MultipartConfig(
		maxFileSize = 1049000 // Max file size 1 mebibit
)
public class TeacherRegistrationPageController extends HttpServlet {

	private static final long serialVersionUID = -5063385726137135766L;
	
	private static final Logger LOGGER = Logger.getLogger(TeacherRegistrationPageController.class);


	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
		
		HttpSession session = httpServletRequest.getSession(false);
		
		Boolean isSuccess = (Boolean) session.getAttribute(Constant.SUCCESS);

		session.removeAttribute(Constant.SUCCESS);
		
		if (isSuccess != null && isSuccess) {
			MessageService.addMessage(
				httpServletRequest,
				"successMessage", 
				"Success, the teacher has been added."
			);
		}
		
		GettingCourseService gettingCourseService = new GettingCourseService(
				new CourseDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		try {
			httpServletRequest.setAttribute(
				"courseList", 
				gettingCourseService.getAll(ArrayList::new)
			);
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		
		try {
			httpServletRequest
				.getRequestDispatcher("/WEB-INF/jsp/admin/teacherRegistration.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doPost");
		
		UserRegistrationService userRegistrationService = new UserRegistrationService(
				new UserDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		try {			
			if (userRegistrationService.registerTeacher(httpServletRequest)) {
				httpServletResponse.sendRedirect(
					httpServletRequest
						.getContextPath()
						 + "/admin/teacherRegistration"
				);
				
				HttpSession session = httpServletRequest.getSession(false);
				
				session.setAttribute(Constant.SUCCESS, true);
			} else {
				doGet(httpServletRequest, httpServletResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
}
