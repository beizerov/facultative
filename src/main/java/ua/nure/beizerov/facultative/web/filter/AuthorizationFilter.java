package ua.nure.beizerov.facultative.web.filter;


import static ua.nure.beizerov.facultative.service.AuthenticationService.getAuthenticatedUser;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.service.util.AuthorizationService;
import ua.nure.beizerov.facultative.web.util.Util;


/**
 * Servlet Filter implementation class AuthorizationFilter
 */
@WebFilter(urlPatterns = {"/admin/*", "/teacher/*", "/student/*"})
public class AuthorizationFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(AuthorizationFilter.class);
	

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(
			ServletRequest request, ServletResponse response, FilterChain chain
	) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		Optional<HttpSession> session = Optional.ofNullable(
				httpRequest.getSession(false)
		);
				
		session.ifPresent(currentSession -> 
			getAuthenticatedUser(currentSession).ifPresentOrElse(authUser -> {
				if (AuthorizationService.isOwner(httpRequest, authUser)) {
					try {
						LOGGER.debug("Authorized user");
						
						chain.doFilter(request, response);
					} catch (IOException | ServletException e) {
						LOGGER.error(e);
					}
				} else {
					LOGGER.debug("Redirect to user's homepage");
					
					redirectToHomePage(httpRequest, httpResponse, authUser);
				}
			}, () -> {
				try {
					LOGGER.debug("Redirect to login page");
					
					Util.redirectToContextPath(httpRequest, httpResponse);
				} catch (IOException e) {
					LOGGER.error(e);
				}
			})
		);
	}


	private void redirectToHomePage(
			HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			User user
	) {
		try {
			String uri = 
				httpRequest.getContextPath()
				+ AuthorizationService.getUserAuthorizationTokenUri(user)
				+ user.getLogin()
			;
			
			httpResponse.sendRedirect(uri);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
}
