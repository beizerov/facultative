package ua.nure.beizerov.facultative.web.filter;


import java.io.IOException;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.dao.UserDaoRdbms;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.service.AuthenticationService;
import ua.nure.beizerov.facultative.web.util.Util;


/**
 * Servlet Filter implementation class AuthorizationFilter
 */
@WebFilter("/login")
public class AuthenticationFilter implements Filter {
	
	private static final Logger LOGGER = Logger.getLogger(AuthenticationFilter.class);
	
	private AuthenticationService authenticationService;

	
	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		ServletContext context = fConfig.getServletContext();
		//	minutes * hours * days ≈ year
		context.setSessionTimeout(60 * 24 * 365);

		authenticationService = new AuthenticationService(
			new UserDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		LOGGER.trace("AuthenticationFilter initialized");
	}
    

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(
			ServletRequest request, ServletResponse response, FilterChain chain
	) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		Optional<User> user =  
				authenticationService
					.authentication(httpRequest)
		;
		
		Optional<HttpSession> session = Optional.ofNullable(
			httpRequest.getSession(false)
		);

		session.ifPresent(currentSession -> 
			user.ifPresentOrElse(authUser -> {
				putUserDataIntoSession(currentSession, authUser);
				LOGGER.debug("User authenticated");
				doNextByChain(request, response, chain);
			}, () -> {
				LOGGER.debug("User failed authentication");
				redirect(httpRequest, httpResponse);
			}
		));	
	}

	
	private void putUserDataIntoSession(HttpSession session, User user) {
		session.setAttribute("user", user);
	}
	
	private void redirect(
			HttpServletRequest httpRequest, HttpServletResponse httpResponse
	) {
		try {
			Util.redirectToContextPath(httpRequest, httpResponse);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	
	private void doNextByChain(
			ServletRequest request,
			ServletResponse response,
			FilterChain chain
	) {
		try {
			chain.doFilter(request, response);
		} catch (IOException | ServletException e) {
			LOGGER.error(e);
		}
	}
}
