package ua.nure.beizerov.facultative.web.controller.student;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;
import ua.nure.beizerov.facultative.persistence.dao.CourseDao;
import ua.nure.beizerov.facultative.persistence.dao.CourseDaoRdbms;
import ua.nure.beizerov.facultative.persistence.dto.CourseDto;
import ua.nure.beizerov.facultative.persistence.enumeration.Order;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.service.AuthenticationService;
import ua.nure.beizerov.facultative.service.EnrollService;
import ua.nure.beizerov.facultative.service.GettingCourseService;


/**
 * Servlet implementation class AdminController
 */
@WebServlet("/student/*")
public class StudentHomePageController extends HttpServlet {

	private static final long serialVersionUID = 1860239447174091340L;
	
	private static final Logger LOGGER = Logger.getLogger(StudentHomePageController.class);

	
	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		GettingCourseService gettingCourseService = new GettingCourseService(
				 new CourseDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		try {
			Collection<CourseDto> courseList = gettingCourseService.getAll(
				StoredProcedureParameterName.COURSE_NAME, Order.ASC, ArrayList::new
			);
			
			httpServletRequest.setAttribute("courses", courseList);
			
			httpServletRequest
				.getRequestDispatcher("/WEB-INF/jsp/student/home.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest, 
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		String courseName = httpServletRequest.getParameter("courseName");
				
		LOGGER.debug(courseName);

		CourseDao courseDao = new CourseDaoRdbms(RdbmsResourceNaming.MY_SQL);
		
		GettingCourseService gettingCourseService = new GettingCourseService(courseDao);
		
		try {			
			Collection<CourseDto> courseList = gettingCourseService.getAll(
				StoredProcedureParameterName.COURSE_NAME, Order.ASC, ArrayList::new
			);
			
			CourseDto course = courseList.stream()
					.findFirst()
					.filter(c -> c.getCourseName().equals(courseName))
					.orElseThrow()
			;
			
			EnrollService enrollService = new EnrollService(courseDao);
			
			User authUser = AuthenticationService.getAuthenticatedUser(
				httpServletRequest.getSession()
			).orElseThrow(); 
			
			enrollService.enrollStudent(
				course.getCourseId(), authUser.getId() 
			);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
}
