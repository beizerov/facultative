package ua.nure.beizerov.facultative.web.listener;


import java.util.HashMap;
import java.util.ListResourceBundle;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.configuration.MySqlDbcpDataSourceConfiguration;
import ua.nure.beizerov.facultative.i18n.ResourceBundleEngGbr;
import ua.nure.beizerov.facultative.i18n.ResourceBundleRus;
import ua.nure.beizerov.facultative.i18n.ResourceBundleUkr;


/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

	private static final Logger LOGGER = Logger.getLogger(ContextListener.class);
	
	
	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
	@Override
    public void contextDestroyed(ServletContextEvent sce)  { 
		log("Servlet context destruction starts");
		// no op
		log("Servlet context destruction finished");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
	@Override
    public void contextInitialized(ServletContextEvent sce)  { 
    	ServletContext servletContext = sce.getServletContext();
    		
    	initMySqlDbcpBasicDataSource();
    	initContextPath(servletContext);
    	initI18N(servletContext);
    }

	
	private void initContextPath(ServletContext servletContext) {
		if (servletContext.getContextPath().isEmpty()) {
			servletContext.setAttribute("contextPath", "/");

			LOGGER.info(servletContext.getContextPath());
		} else {
			servletContext.setAttribute(
				"contextPath", servletContext.getContextPath()
			);

			LOGGER.info(servletContext.getContextPath());
		}
	}
	
	/**
	 * Initializes the BasicDataSource pool for MySQL
 	 * and binds "jdbc/facultiveDB" to the context via JNDI.
 	 * 
 	 * @see MySqlDbcpDataSourceConfiguration
	 */
	private void initMySqlDbcpBasicDataSource() {
		MySqlDbcpDataSourceConfiguration.initDataSource();
	}
	
	/**
	 * Initializes i18n subsystem.
	 */
	private void initI18N(ServletContext servletContext) {
		LOGGER.debug("I18N subsystem initialization started");
		
		Map<String, ? super ListResourceBundle> i18n = new HashMap<>();
		
		i18n.put("eng", new ResourceBundleEngGbr());
		i18n.put("rus", new ResourceBundleRus());
		i18n.put("ukr", new ResourceBundleUkr());
		
		servletContext.setAttribute(
			"i18n", i18n
		);
		
		LOGGER.debug("I18N subsystem initialization finished");
	}
	
	private void log(String msg) {
		LOGGER.info("[ContextListener] " + msg);
	}
}
