package ua.nure.beizerov.facultative.web.controller;


import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.service.AuthenticationService;
import ua.nure.beizerov.facultative.service.util.AuthorizationService;


@WebServlet("/login")
public class LoginController extends HttpServlet {

	private static final long serialVersionUID = 9202281843976776122L;
	
	private static final Logger LOGGER = Logger.getLogger(LoginController.class);
	

	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		Optional<HttpSession> session = Optional.ofNullable(
				httpServletRequest.getSession(false)
		);
				
		session.ifPresent(currentSession -> 
			AuthenticationService
				.getAuthenticatedUser(currentSession).ifPresent(authUser -> 
					redirect(httpServletRequest, httpServletResponse, authUser)
			)
		);
		LOGGER.debug("doPost");
	}
	
	
	private void redirect(
			HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			User user
	) {
		try {
			if (user != null) {
				String uri = httpRequest.getContextPath()
					+ AuthorizationService.getUserAuthorizationTokenUri(user)
					+ user.getLogin()
				;
				
				httpResponse.sendRedirect(uri);
			} else {
				httpResponse.sendError(
						HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized"
				);
			}
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
}
