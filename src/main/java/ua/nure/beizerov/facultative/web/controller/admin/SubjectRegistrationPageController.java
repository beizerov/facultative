package ua.nure.beizerov.facultative.web.controller.admin;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class SubjectRegistrationPageController
 */
@WebServlet("/admin/subjectRegistration")
public class SubjectRegistrationPageController extends HttpServlet {

	private static final long serialVersionUID = -1115202501703638014L;

	private static final Logger LOGGER = Logger.getLogger(SubjectRegistrationPageController.class);
	

	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
		
		try {
			httpServletRequest
				.getRequestDispatcher("/WEB-INF/jsp/admin/subjectRegistration.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		doGet(httpServletRequest, httpServletResponse);
	}
}
