package ua.nure.beizerov.facultative.web.controller;


import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class Logout
 */
@WebServlet("/logout")
public class LogoutController extends HttpServlet {
	
	private static final long serialVersionUID = 6375780354900540667L;
	
	private static final Logger LOGGER = Logger.getLogger(LogoutController.class);
	
	
	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		String logout = httpServletRequest.getParameter("logout");
		
		if ("logout".equals(logout)) {
			Optional<HttpSession> session = Optional.ofNullable(
				httpServletRequest.getSession(false)
			);

			session.ifPresent(currentSession -> {
				currentSession.invalidate();
				
				try {
					httpServletResponse.sendRedirect(
						httpServletRequest
							.getServletContext()
							.getAttribute("contextPath")
							.toString()
					);
				} catch (IOException e) {
					LOGGER.error(e);
				}
			});
			
			LOGGER.debug(
				"ContextPath: " 
				+ httpServletRequest
					.getServletContext()
					.getAttribute("contextPath")
					.toString()
			);
		}
	}
}
