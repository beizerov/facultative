package ua.nure.beizerov.facultative.web.controller.admin;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.persistence.dao.CourseDaoRdbms;
import ua.nure.beizerov.facultative.persistence.dao.SubjectDaoRdbms;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.service.CourseRegistrationService;
import ua.nure.beizerov.facultative.service.GettingSubjectService;
import ua.nure.beizerov.facultative.service.util.MessageService;
import ua.nure.beizerov.facultative.web.constant.Constant;


/**
 * Servlet implementation class TeacherRegistrationPageController
 */
@WebServlet("/admin/courseRegistration")
public class CourseRegistrationPageController extends HttpServlet {

	private static final long serialVersionUID = -7827293767306268752L;

	private static final Logger LOGGER = Logger.getLogger(CourseRegistrationPageController.class);

	
	/**
	 * @see HttpServlet#doGet(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doGet(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doGet");
		
		HttpSession session = httpServletRequest.getSession(false);
		
		Boolean isSuccess = (Boolean) session.getAttribute(Constant.SUCCESS);

		session.removeAttribute(Constant.SUCCESS);
		
		if (isSuccess != null && isSuccess) {
			MessageService.addMessage(
				httpServletRequest,
				"successMessage", 
				"Success, the course has been added."
			);
		}
		
		GettingSubjectService gettingSubjectService = new GettingSubjectService(
				new SubjectDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		try {
			httpServletRequest.setAttribute(
				"subjects",
				gettingSubjectService.getAll(ArrayList::new)	
			);
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		
		try {
			httpServletRequest
				.getRequestDispatcher("/WEB-INF/jsp/admin/courseRegistration.jsp")
				.forward(httpServletRequest, httpServletResponse)
			;
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(
	 * 			HttpServletRequest httpServletRequest,
	 * 			HttpServletResponse httpServletResponse
	 * )
	 */
	@Override
	protected void doPost(
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse
	) throws ServletException, IOException {
		LOGGER.debug("doPost");

		CourseRegistrationService courseRegistrationService = new CourseRegistrationService(
				new CourseDaoRdbms(RdbmsResourceNaming.MY_SQL)
		);
		
		try {			
			if (courseRegistrationService.registerCourse(httpServletRequest)) {
			
				httpServletResponse.sendRedirect(
					httpServletRequest
						.getContextPath()
					+ "/admin/courseRegistration"
				);
			
				HttpSession session = httpServletRequest.getSession(false);
			
				session.setAttribute(Constant.SUCCESS, true);
			} else {
				doGet(httpServletRequest, httpServletResponse);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
}
