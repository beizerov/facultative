package ua.nure.beizerov.facultative.web.constant;


public final class Constant {

	private Constant() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	
	public static final String SUCCESS = "success";
}
