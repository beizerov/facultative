package ua.nure.beizerov.facultative.web.util;


import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.persistence.dto.CourseDto;


public final class PrintCourseDtoTag extends TagSupport {
	
	private static final long serialVersionUID = 6795197770195312594L;
	
	private static final Logger LOGGER = Logger.getLogger(
			PrintCourseDtoTag.class
	);
			
	private CourseDto courseDto;
	
	
	public void setCourse(CourseDto courseDto) {
		this.courseDto = courseDto;
	}
	

	@Override
	public int doStartTag() throws JspException {
		StringBuilder out = new StringBuilder();
		
		final String startTagTr = "<tr>"; 
		final String endTagTr = "</tr>";
		final String startTagTd = "<td"; 
		final String endTagTd = "</td>";
		
		out
			.append(startTagTr)
			
			.append(
				startTagTd 
				+ " id='courseName' name='courseName' value='courseName' class='course'>"
			)
			.append(courseDto.getCourseName())
			.append(endTagTd)
			.append(
				startTagTd
				+ " id='startDate' name='startDate' class='course'>"
			)
			.append(courseDto.getStartDate())
			.append(endTagTd)
			.append(
				startTagTd
				+ " id='endDate' name='endDate' class='course'>"
			)
			.append(courseDto.getEndDate())
			.append(endTagTd)
			.append(
				startTagTd
				+ " id='duration' name='duration' class='course'>"
			)
			.append(courseDto.getDuration())
			.append(endTagTd)
			.append(
					startTagTd
					+ " id='numberOfstudents'"
					+ " name='NumberOfstudents' class='course'>"
			)
			.append(courseDto.getNumberOfstudents())
			.append(endTagTd)
			.append(
					startTagTd
					+ " id='description' name='description' class='course'>"
			)
			.append(/* courseDto.getDescription() */ 
					"<a href=\"#\">Description</a>"
			)
			.append(endTagTd)
			.append(
					startTagTd
					+ " id='subjectName' name='subjectName' class='course'>"
			)
			.append(courseDto.getSubjectName())
			.append(endTagTd)
			.append(
					startTagTd
					+ " id='teacherFullName'"
					+ " name='teacherFullName' class='course'>"
			)
			.append(courseDto.getTeacherFullName())
			.append(endTagTd)
			
			.append(endTagTr)
			.append(
					startTagTd+ " class='course'>"
					+
					"<button type=\"submit\" form=\"form3\" value=\"Submit\">Submit</button>"
			)
			.append(courseDto.getTeacherFullName())
			.append(endTagTd)
			
		;
		
		try {
			pageContext.getOut().println(out);
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		return super.doStartTag();
	}
}
