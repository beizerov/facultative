package ua.nure.beizerov.facultative.persistence.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;


/**
 * Extracts a course from the result set row.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class CourseMapper implements EntityMapper<Course> {

	@Override
	public Course mapRow(ResultSet resultSet) throws SQLException {
		Course course = new Course();
		
		course.setId(
			resultSet.getLong(StoredProcedureParameterName.COURSE_ID)
		);
		course.setName(
			resultSet.getString(StoredProcedureParameterName.COURSE_NAME)
		);
		course.setStartDate(
			resultSet.getDate(
				StoredProcedureParameterName.COURSE_START_DATE
			).toLocalDate()
		);
		course.setEndDate(
			resultSet.getDate(
				StoredProcedureParameterName.COURSE_END_DATE
			).toLocalDate()
		);
		
		return course;
	}
}
