package ua.nure.beizerov.facultative.persistence.mapper;


import ua.nure.beizerov.facultative.persistence.dto.Dto;


/**
 * <p>
 * 	Defines general contract for mapping database result set rows 
 * 	to application DTO.
 * </p>
 * <p>
 * 	Implementations are not supposed to move cursor of the resultSet via next() 
 * 	method, but only extract information from the row 
 * 	in current cursor position.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <E> All classes/interfaces which implements/extends 
 * {@link Dto} interface.
 * 
 * @see Mapper
 */
public interface DtoMapper<E extends Dto> extends Mapper<E> {

}
