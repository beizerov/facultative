package ua.nure.beizerov.facultative.persistence.dao;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.configuration.DataSourceManagement;
import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.model.Grade;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;
import ua.nure.beizerov.facultative.persistence.dto.CourseDto;
import ua.nure.beizerov.facultative.persistence.enumeration.Order;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.persistence.exception.constant.SqlExceptionMessage;
import ua.nure.beizerov.facultative.persistence.mapper.CourseDtoMapper;
import ua.nure.beizerov.facultative.persistence.mapper.CourseMapper;
import ua.nure.beizerov.facultative.persistence.util.Util;


/**
 * An implementation of {@link CourseDao} that persists users in RDBMS.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class CourseDaoRdbms implements CourseDao {
	
	private static final Logger LOGGER = Logger.getLogger(CourseDaoRdbms.class);
	
	private final DataSourceManagement dataSourceManagement;
	private final CourseDtoMapper courseDtoMapper;
	private final CourseMapper courseMapper;

	
	public CourseDaoRdbms(RdbmsResourceNaming rdbmsResourceNaming) {
		this.dataSourceManagement = new DataSourceManagement(rdbmsResourceNaming);
		this.courseDtoMapper = new CourseDtoMapper();
		this.courseMapper = new CourseMapper();
	}
	

	/**
	 * Creates a data record in the storage system. 
	 * Saves course data.
	 * 
	 *
	 * @param course course
	 * @throws SQLException if any SQL error occurs
	 */
	@Override
	public void create(Course course) throws SQLException {
		final String sqlQuery = "{call save_course(?, ?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_NAME, course.getName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_START_DATE,
				course.getStartDate().toString()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_END_DATE,
				course.getEndDate().toString()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_DESCRIPTION,
				course.getDescription()
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.COURSE_HAS_NOT_BEEN_CREATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.COURSE_HAS_NOT_BEEN_CREATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.COURSE_HAS_NOT_BEEN_CREATED, e
			);
		}
	}
	
	@Override
	public void createForSelectedSubject(Course course, long subjectId) 
			throws SQLException {
		final String sqlQuery = "{call save_course_for_selected_subject(?, ?, ?, ?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_NAME, course.getName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_START_DATE,
				course.getStartDate().toString()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_END_DATE,
				course.getEndDate().toString()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_DESCRIPTION,
				course.getDescription()
			);
			callableStatement.setLong(
				StoredProcedureParameterName.SUBJECT_ID, subjectId
			);
			
			callableStatement.registerOutParameter(
				StoredProcedureParameterName.OUT_PARAM_IS_OK,
				Types.INTEGER
			);
			
			LOGGER.trace(sqlQuery);
			
			callableStatement.execute();
			
			int isOk = callableStatement.getInt(
				StoredProcedureParameterName.OUT_PARAM_IS_OK
			); 
			
			if (isOk != 1) {
				throw new SQLException(
					SqlExceptionMessage.COURSE_HAS_NOT_BEEN_CREATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.COURSE_HAS_NOT_BEEN_CREATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.COURSE_HAS_NOT_BEEN_CREATED, e
			);
		}
	}

	@Override
	public void delete(long id) throws SQLException {
		final String sqlQuery = "{call delete_course_by_id(?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID, id
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.COURSE_HAS_NOT_BEEN_DELETED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.COURSE_HAS_NOT_BEEN_DELETED, e);
			
			throw new SQLException(
				SqlExceptionMessage.COURSE_HAS_NOT_BEEN_DELETED, e
			);
		}
	}

	@Override
	public void update(Course course) throws SQLException {
		final String sqlQuery = "{call update_course(?, ?, ?, ?, ?)}"; 
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID, course.getId()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_NAME, course.getName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_START_DATE,
				course.getStartDate().toString()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_END_DATE,
				course.getEndDate().toString()
			);
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_DESCRIPTION,
				course.getDescription()
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.COURSE_HAS_NOT_BEEN_UPDATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.COURSE_HAS_NOT_BEEN_UPDATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.COURSE_HAS_NOT_BEEN_UPDATED, e
			);
		}
	}
	
	@Override
	public Stream<Course> getAll() throws SQLException {
		final String sqlQuery = "{call get_courses()}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<Course>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					courseMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
		}
	}

	/**
	 * Get all CourseDto objects as Java Stream.
	 * 
	 * 
	 * @return a lazily populated stream of CourseDto objects. 
	 * 		Note the stream returned must be closed to free
	 * 		all the acquired resources.
	 * 		The stream keeps an open connection to the database till it is 
	 * 		complete or is closed manually.
	 */
	@Override
	public Stream<CourseDto> getAll(String key, Order order) 
			throws SQLException {
		final String sqlQuery = "{call get_courses_sorted_by_column_name_and_order_by(?, ?)}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			callableStatement.setString(1, key);
			callableStatement.setString(
				StoredProcedureParameterName.SORT_ORDER, order.name()
			);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<CourseDto>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					courseDtoMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
		}
	}
	
	/**
	 * Get CourseDto objects by subject id as Java Stream.
	 * 
	 * 
	 * @return a lazily populated stream of CourseDto objects by subject id. 
	 * 		Note the stream returned must be closed to free all the acquired 
	 * 		resources.
	 * 		The stream keeps an open connection to the database till it is 
	 * 		complete or is closed manually.
	 */
	@Override
	public Stream<CourseDto> getAllBySubjectId(
			long subjectId, String key, Order order
	) throws SQLException {
		final String sqlQuery = "{call get_courses_by_subject_id_sorted_by_column_name_and_order_by(?, ?, ?)}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			callableStatement.setLong(
				StoredProcedureParameterName.SUBJECT_ID, subjectId
			);
			callableStatement.setString(2, key);
			callableStatement.setString(
				StoredProcedureParameterName.SORT_ORDER, order.name()
			);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<CourseDto>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					courseDtoMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
		}
	}
	
	/**
	 * Get CourseDto objects by teacher id as Java Stream.
	 * 
	 * 
	 * @return a lazily populated stream of CourseDto objects by 
	 * 		teacher id. Note the stream returned must be closed to free
	 * 		all the acquired resources. 
	 * 		The stream keeps an open connection to the database 
	 * 		till it is complete or is closed manually.
	 */
	@Override
	public Stream<CourseDto> getAllByTeacherId(
			long teacherId, String key, Order order
	) throws SQLException {
		final String sqlQuery = "{call get_courses_by_teacher_id_sorted_by_col_name_and_order_by(?, ?, ?)}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			callableStatement.setLong(
				StoredProcedureParameterName.TEACHER_ID, teacherId
			);
			callableStatement.setString(2, key);
			callableStatement.setString(
				StoredProcedureParameterName.SORT_ORDER, order.name()
			);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<CourseDto>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					courseDtoMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
		}
	}	

	/**
	 * Get CourseDto objects by student id as Java Stream.
	 * 
	 * 
	 * @return a lazily populated stream of CourseDto objects by student id. 
	 * 		Note the stream returned must be closed to free all the acquired 
	 * 		resources. 
	 * 		The stream keeps an open connection to the database 
	 * 		till it is complete or is closed manually.
	 */
	@Override
	public Stream<CourseDto> getAllByStudentId(long studentId)
			throws SQLException {
		final String sqlQuery = "{call get_student_courses_by_student_id(?)}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			callableStatement.setLong(
				StoredProcedureParameterName.STUDENT_ID, studentId
			);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<CourseDto>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					courseDtoMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_COURSE_DTO_OBJECTS, e
			);
		}
	}

	
	@Override
	public void enrollStudent(long studentId, long courseId) 
			throws SQLException {
		final String sqlQuery = "{call enroll_student(?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.STUDENT_ID, studentId
			);
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID, courseId
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.STUDENT_NOT_ENROLLED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.STUDENT_NOT_ENROLLED, e);
			
			throw new SQLException(
				SqlExceptionMessage.STUDENT_NOT_ENROLLED, e
			);
		}
	}

	@Override
	public void assignTeacher(long teacherId, long courseId) 
			throws SQLException {
		final String sqlQuery = "{call assign_teacher(?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.TEACHER_ID, teacherId
			);
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID,	courseId
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.TEACHER_NOT_ENROLLED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.TEACHER_NOT_ENROLLED, e);
			
			throw new SQLException(
				SqlExceptionMessage.TEACHER_NOT_ENROLLED, e
			);
		}
	}

	@Override
	public void rateStudent(long courseId, long studentId, Grade grade)
			throws SQLException {
		final String sqlQuery = "{call rate_student(?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID, courseId
			);
			callableStatement.setLong(
				StoredProcedureParameterName.STUDENT_ID, studentId
			);
			callableStatement.setString(
				StoredProcedureParameterName.GRADE, grade.getLetterGrade()
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.STUDENT_HAS_NOT_BEEN_RATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.STUDENT_HAS_NOT_BEEN_RATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.STUDENT_HAS_NOT_BEEN_RATED, e
			);
		}
	}

	@Override
	public void editStudentGrade(long courseId, long studentId, Grade grade)
			throws SQLException {
		final String sqlQuery = "{call update_student_grade(?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID, courseId
			);
			callableStatement.setLong(
				StoredProcedureParameterName.STUDENT_ID, studentId
			);
			callableStatement.setString(
				StoredProcedureParameterName.GRADE, grade.getLetterGrade()
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.STUDENT_GRADE_NOT_UPDATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.STUDENT_GRADE_NOT_UPDATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.STUDENT_GRADE_NOT_UPDATED, e
			);
		}
	}

	@Override
	public Optional<Course> getByName(String courseName) throws SQLException {
		final String sqlQuery = "{call get_course_by_name(?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			Optional<Course> course = Optional.empty();
			
			callableStatement.setString(
				StoredProcedureParameterName.COURSE_NAME, courseName
			);
			
			try(ResultSet resultSet = callableStatement.executeQuery()) {
				if (resultSet.next()) {
					course = Optional.ofNullable(courseMapper.mapRow(resultSet));
				}
			}
			
			return course;
		} catch (Exception e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_COURSE_BY_NAME, e);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_COURSE_BY_NAME, e
			);
		}
	}
}
