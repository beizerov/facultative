package ua.nure.beizerov.facultative.persistence.dto;


import ua.nure.beizerov.facultative.persistence.Data;


/**
 * Superinterface for all classes that are classes of data transfer objects.
 * 
 * @author Oleksii Beizerov
 *
 */
public interface Dto extends Data {

}
