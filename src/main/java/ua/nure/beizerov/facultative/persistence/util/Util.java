package ua.nure.beizerov.facultative.persistence.util;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.persistence.Data;
import ua.nure.beizerov.facultative.persistence.exception.constant.SqlExceptionMessage;


/**
 * This class is represent a Utility.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public final class Util {
	
	private Util() {
		throw new IllegalStateException("Util class");
	}
	
	
	/**
	 * Create a stream.
	 * 
	 * In overriding method  {@link Spliterator#tryAdvance(Consumer)} 
	 * can be thrown unchecked exception 
	 * {@link UncheckedSqlFacultativeApplicationException}.
	 * 
	 * 
	 * @param <T> all type that extends {@link Data}
	 * @param callableStatement reference {@link CallableStatement} instance
	 * @param resultSet reference {@link ResultSet} instance
	 * @param function reference {@link Function} instance
	 * @param logger reference {@link Logger} instance
	 * @return stream of T type objects 
	 */
	public static <T extends Data> Stream<T> createStreamOf(
			Connection connection,
			CallableStatement callableStatement,
			ResultSet resultSet,
			Function<ResultSet, T> function,
			Logger logger
	) {
		return StreamSupport.stream(
				new Spliterators.AbstractSpliterator<T>(
					Integer.MAX_VALUE, Spliterator.ORDERED
				) {
	
					@Override
					public boolean tryAdvance(
							Consumer<? super T> action
					) {
						try {
							if (resultSet.next()) {
								action.accept(
									function.apply(resultSet)
								);
								
								return true;
							}
						} catch (SQLException e) {
							logger.error(
								SqlExceptionMessage.CANNOT_GET_OBJECTS, e
							);
						}
						
						return false;
					}
				}, false
			).onClose(
				() -> closeAfterCloseStream(
						connection, callableStatement, resultSet, logger
			));
	}
	
	private static void closeAfterCloseStream(
			Connection connection,
			CallableStatement callableStatement, 
			ResultSet resultSet,
			Logger log
	) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (callableStatement != null) {
				callableStatement.close();
			}
			
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			log.error(SqlExceptionMessage.CANNOT_CLOSE_RESOURCES, e);
		}
	}
}
