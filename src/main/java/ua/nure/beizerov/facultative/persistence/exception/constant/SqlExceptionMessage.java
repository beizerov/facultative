package ua.nure.beizerov.facultative.persistence.exception.constant;


/**
 * @author Oleksii Beizerov
 *
 */
public final class SqlExceptionMessage {

	
	private SqlExceptionMessage() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	
	// For connection
	public static final String
	CONNECTION_NOT_ESTABLISHED = "Connection not established";
	
	
	// For objects stream
	public static final String 
	CANNOT_GET_OBJECTS = "Can't get objects";
	
	
	// For users
	public static final String
	CANNOT_GET_USER_BY_ID = "Can't get user by id";
	
	public static final String
	CANNOT_GET_USER_BY_LOGIN = "Can't get user by login";
	
	public static final String
	CANNOT_GET_USER_BY_EMAIL = "Can't get user by email";
	
	public static final String
	USER_HAS_NOT_BEEN_CREATED = "User has not been created";
	
	public static final String
	USER_HAS_NOT_BEEN_DELETED = "User has not been deleted";
	
	public static final String
	USER_ACTIVITY_STATE_NOT_CHANGED = "User activity status not changed";
	
	public static final String
	CANNOT_GET_THE_USER_OBJECTS = "Can't get the User objects";
	
	
	// For courses
	public static final String
	COURSE_HAS_NOT_BEEN_CREATED = "Course has not been created";
	
	public static final String
	COURSE_HAS_NOT_BEEN_DELETED = "Course has not been deleted";
	
	public static final String
	COURSE_HAS_NOT_BEEN_UPDATED = "Course has not been updated";
	
	public static final String
	CANNOT_GET_COURSE_BY_NAME = "Cannot get the course by name";
	
	public static final String
	CANNOT_GET_THE_COURSE_DTO_OBJECTS = "Can't get the CourseDto objects";
	
	public static final String
	CANNOT_CLOSE_RESOURCES = "Can't close resources";
	
	public static final String
	STUDENT_NOT_ENROLLED = "Student enroll record not added to database";
	
	public static final String
	STUDENT_HAS_NOT_BEEN_RATED = "Student has not been rated";
	public static final String
	STUDENT_GRADE_NOT_UPDATED = "Student grade not updated";
	
	public static final String
	TEACHER_NOT_ENROLLED = "Teacher enroll record not added to database";
	
	
	// for subjects
	public static final String
	SUBJECT_HAS_NOT_BEEN_CREATED = "Subject has not been created";
	
	public static final String
	SUBJECT_HAS_NOT_BEEN_DELETED = "Subject has not been deleted";
	
	public static final String
	SUBJECT_HAS_NOT_BEEN_UPDATED = "Subject has not been updated";
	
	public static final String
	CANNOT_GET_THE_SUBJECT_OBJECTS = "Can't get the Subject objects";
}
