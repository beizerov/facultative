package ua.nure.beizerov.facultative.persistence.dao;


import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.model.Grade;
import ua.nure.beizerov.facultative.persistence.dto.CourseDto;
import ua.nure.beizerov.facultative.persistence.enumeration.Order;


/**
 * <p>
 * 	Defines a contract for classes that implement this interface. 
 * 	CourseDao is a data access interface for implementations of 
 * 	various storage systems.
 * </p>
 * <p>
 * 	Get all courses from view_courses.
 * 	view_courses it's the view in database.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @see Dao
 */
public interface CourseDao extends Dao<Course> {
	
	/**
	 * Creates a data record in the storage system 
	 * and registers the course for the selected subject.
	 * 
	 * 
	 * @param course an object of a class that extends the class Course
	 * @param subjectId subject id
 	 * @throws SQLException if any SQL error occurs
	 */
	void createForSelectedSubject(Course course, long subjectId)
			throws SQLException;
	
	/**
	 * Get a course by name
	 * 
	 * 
	 * @param courseName course name
	 * @return an optional with course if a course with <code>courseName</code> 
	 * 		exists, empty optional otherwise
	 * @throws SQLException if any SQL error occurs
	 */
	Optional<Course> getByName(String courseName) throws SQLException;
	
	/**
	 * Get all CourseDto objects.
	 * 
	 * 
	 * @param key column name or key in NoSQL of according to which 
	 * 		the data is needed to be arranged
	 * @param order the sort order
	 * 		for sorting the returned data
	 * @return all the CourseDto objects as a stream. The stream may be lazily 
	 * 		or eagerly evaluated based on the implementation. 
	 * 		The stream must be closed after use.
	 * 
	 * @throws SQLException if any SQL error occurs
	 */
	Stream<CourseDto> getAll(String key, Order order) throws SQLException;
	
	/**
	 * Get CourseDto objects by subject id.
	 * 
	 * 
	 * @param subjectId subject id
	 * @param key column name or key in NoSQL of according to which 
	 * 		the data is needed to be arranged
	 * @param order the sort order for sorting the returned data
	 * @return all the CourseDto objects by subject name as a stream. 
	 * 		The stream may be lazily or eagerly evaluated based on 
	 * 		the implementation. The stream must be closed after use.
	 * 
	 * @throws SQLException if any SQL error occurs
	 */
	Stream<CourseDto> getAllBySubjectId(long subjectId, String key, Order order)
			throws SQLException;
	
	/**
	 * Get CourseDto objects by teacher id.
	 * 
	 * 
	 * @param teacherId teacher id
	 * @param key column name or key in NoSQL of according to which 
	 * 		the data is needed to be arranged
	 * @param order the sort order for sorting the returned data
	 * @return the CourseDto objects by teacher full name as a stream. 
	 * 		The stream may be lazily or eagerly evaluated based on 
	 * 		the implementation. The stream must be closed after use.
	 * 
	 * @throws SQLException if any SQL error occurs
	 */
	Stream<CourseDto> getAllByTeacherId(long teacherId, String key, Order order)
			throws SQLException;
	
	/**
	 * Get CourseDto objects by student id.
	 * 
	 * 
	 * @param studentId student id
	 * @return the CourseDto objects by student id as a stream. 
	 * 		The stream may be lazily or eagerly evaluated based on 
	 * 		the implementation. The stream must be closed after use.
	 * 
	 * @throws SQLException if any SQL error occurs
	 */
	Stream<CourseDto> getAllByStudentId(long studentId) throws SQLException;
	
	/**
	 * Enroll student in a course.
	 * 
	 * 
	 * @param studentId student id
	 * @param courseId course id
	 * @throws SQLException if any SQL error occurs
	 */
	void enrollStudent(long studentId, long courseId) throws SQLException;
	
	/**
	 * Assign teacher to course.
	 * 
	 * 
	 * @param teacherId teacher id
	 * @param courseId course id
	 * @throws SQLException if any SQL error occurs
	 */
	void assignTeacher(long teacherId, long courseId) throws SQLException;
	
	/**
	 * Rate student.
	 * 
	 * 
	 * @param courseId course id
	 * @param studentId student id
	 * @param grade grade
	 * @throws SQLException if any SQL error occurs
	 */
	void rateStudent(long courseId, long studentId, Grade grade) 
			throws SQLException;
	
	/**
	 * Change student grade.
	 * 
	 * 
	 * @param courseId course id
	 * @param studentId student id
	 * @param grade grade
	 * @throws SQLException if any SQL error occurs
	 */
	void editStudentGrade(long courseId, long studentId, Grade grade) 
			throws SQLException;
}
