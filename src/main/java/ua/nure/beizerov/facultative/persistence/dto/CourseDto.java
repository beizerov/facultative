package ua.nure.beizerov.facultative.persistence.dto;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;


/**
 * <p>
 * The class represents the data transfer objects of the Course 
 * and the data associated with it.
 * </p>
 * <p>
 * Used to transfer convenience and optimize data transfer between tiers.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class CourseDto implements Dto {
	
	private static final long serialVersionUID = 6007056764054836930L;
	
	private long courseId;
	private String courseName;
	private String startDate;
	private String endDate;
	private int duration;
	private long numberOfstudents;
	private String description;
	private String subjectName;
	private String teacherFullName;
	
	
	public long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	
	public String getCourseName() {
		return courseName;
	}
	
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate.format(
			DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
		);
	}
	
	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate.format(
			DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
		);
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public long getNumberOfstudents() {
		return numberOfstudents;
	}
	
	public void setNumberOfstudents(long numberOfstudents) {
		this.numberOfstudents = numberOfstudents;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getSubjectName() {
		return subjectName;
	}
	
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	public String getTeacherFullName() {
		return teacherFullName;
	}
	
	public void setTeacherFullName(String teacherFullName) {
		this.teacherFullName = teacherFullName;
	}
}
