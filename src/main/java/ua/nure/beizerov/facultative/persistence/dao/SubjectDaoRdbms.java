package ua.nure.beizerov.facultative.persistence.dao;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Stream;
import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.configuration.DataSourceManagement;
import ua.nure.beizerov.facultative.model.Subject;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.persistence.exception.constant.SqlExceptionMessage;
import ua.nure.beizerov.facultative.persistence.mapper.SubjectMapper;
import ua.nure.beizerov.facultative.persistence.util.Util;


public class SubjectDaoRdbms implements SubjectDao {
	
	private static final Logger LOGGER = Logger.getLogger(SubjectDaoRdbms.class);
	
	private final DataSourceManagement dataSourceManagement;
	private final SubjectMapper subjectMapper;
	
	
	public SubjectDaoRdbms(RdbmsResourceNaming rdbmsResourceNaming) {
		this.dataSourceManagement = new DataSourceManagement(rdbmsResourceNaming);
		this.subjectMapper = new SubjectMapper();
	}


	@Override
	public void create(Subject subject) throws SQLException {
		final String sqlQuery = "{call save_subject(?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setString(
				StoredProcedureParameterName.SUBJECT_NAME,
				subject.getName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.SUBJECT_DESCRIPTION,
				subject.getDescription()
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_CREATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_CREATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_CREATED, e
			);
		}
	}

	@Override
	public void delete(long id) throws SQLException {
		final String sqlQuery = "{call delete_subject_by_id(?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
					StoredProcedureParameterName.SUBJECT_ID, id
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_DELETED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_DELETED, e);
			
			throw new SQLException(
				SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_DELETED, e
			);
		}
	}

	@Override
	public void update(Subject subject) throws SQLException {
		final String sqlQuery = "{call update_subject(?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(
				StoredProcedureParameterName.SUBJECT_ID, subject.getId());
			callableStatement.setString(
				StoredProcedureParameterName.SUBJECT_NAME, subject.getName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.SUBJECT_DESCRIPTION,
				subject.getDescription()
			);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_UPDATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_UPDATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.SUBJECT_HAS_NOT_BEEN_UPDATED, e
			);
		}
	}

	@Override
	public Stream<Subject> getAll() throws SQLException {
		final String sqlQuery = "{call get_subjects()}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<Subject>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					subjectMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_THE_SUBJECT_OBJECTS, e);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_SUBJECT_OBJECTS, e
			);
		}
	}
}
