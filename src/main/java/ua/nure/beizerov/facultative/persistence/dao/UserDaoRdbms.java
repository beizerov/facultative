package ua.nure.beizerov.facultative.persistence.dao;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.configuration.DataSourceManagement;
import ua.nure.beizerov.facultative.model.Role;
import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;
import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;
import ua.nure.beizerov.facultative.persistence.exception.constant.SqlExceptionMessage;
import ua.nure.beizerov.facultative.persistence.mapper.UserMapper;
import ua.nure.beizerov.facultative.persistence.util.Util;


/**
 * An implementation of {@link UserDao} that persists users in RDBMS.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class UserDaoRdbms implements UserDao {
	
	private static final Logger LOGGER = Logger.getLogger(UserDaoRdbms.class);
	
	private final DataSourceManagement dataSourceManagement;
	private final UserMapper userMapper;
	
	
	public UserDaoRdbms(RdbmsResourceNaming rdbmsResourceNaming) {
		this.dataSourceManagement = new DataSourceManagement(rdbmsResourceNaming);
		this.userMapper = new UserMapper();
	}
	
	
	@Override
	public void create(User user) throws SQLException {
		final String sqlQuery = "{call save_user(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setInt(
				StoredProcedureParameterName.USER_ACTIVE, user.getActive()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_EMAIL, user.getEmail()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_LOGIN, user.getLogin()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_FIRST_NAME, user.getFirstName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_LAST_NAME, user.getLastName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_PASSWORD, user.getPassword()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_ROLE, user.getRole().name()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_LOCALE, user.getLocale()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_PATH_TO_PHOTO,
				user.getPathToPhoto()
			);

			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) {
				throw new SQLException(
					SqlExceptionMessage.USER_HAS_NOT_BEEN_CREATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.USER_HAS_NOT_BEEN_CREATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.USER_HAS_NOT_BEEN_CREATED, e
			);
		}
	}

	@Override
	public void createTeacherForSelectedCourse(User user, long courseId)
			throws SQLException {
		if (user.getRole() != Role.TEACHER) {
			throw new IllegalArgumentException("User must be a teacher");
		}
		
		final String sqlQuery = "{call save_teacher_for_selected_course(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setInt(
				StoredProcedureParameterName.USER_ACTIVE, user.getActive()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_EMAIL, user.getEmail()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_LOGIN, user.getLogin()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_FIRST_NAME, user.getFirstName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_LAST_NAME, user.getLastName()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_PASSWORD, user.getPassword()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_ROLE, user.getRole().name()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_LOCALE, user.getLocale()
			);
			callableStatement.setString(
				StoredProcedureParameterName.USER_PATH_TO_PHOTO,
				user.getPathToPhoto()
			);
			callableStatement.setLong(
				StoredProcedureParameterName.COURSE_ID, courseId
			);
				
			callableStatement.registerOutParameter(
				StoredProcedureParameterName.OUT_PARAM_IS_OK,
				Types.INTEGER
			);
			
			LOGGER.trace(sqlQuery);
			
			callableStatement.execute();
			
			int isOk = callableStatement.getInt(
				StoredProcedureParameterName.OUT_PARAM_IS_OK
			); 
			
			if (isOk != 1) {
				throw new SQLException(
					SqlExceptionMessage.USER_HAS_NOT_BEEN_CREATED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.USER_HAS_NOT_BEEN_CREATED, e);
			
			throw new SQLException(
				SqlExceptionMessage.USER_HAS_NOT_BEEN_CREATED, e
			);
		}
	}
	
	@Override
	public void delete(long id) throws SQLException {
		final String sqlQuery = "{call delete_user_by_id(?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(StoredProcedureParameterName.USER_ID, id);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) { 
				throw new SQLException(
					SqlExceptionMessage.USER_HAS_NOT_BEEN_DELETED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.USER_HAS_NOT_BEEN_DELETED, e);
			
			throw new SQLException(
				SqlExceptionMessage.USER_HAS_NOT_BEEN_DELETED, e
			);
		}
	}

	@Override
	public Optional<User> getById(long id) throws SQLException {
		final String sqlQuery = "{call get_user_by_id(?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			Optional<User> user = Optional.empty();
			
			callableStatement.setLong(StoredProcedureParameterName.USER_ID, id);
			
			try (ResultSet resultSet = callableStatement.executeQuery()) {
				if (resultSet.next()) {
					user = Optional.ofNullable(userMapper.mapRow(resultSet));
				}
			}
			
			LOGGER.trace(sqlQuery);
			
			return user;
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_USER_BY_ID, e);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_USER_BY_ID, e
			);
		}
	}

	@Override
	public void setActive(long userId, int active) throws SQLException {
		final String sqlQuery = "{call set_user_active(?, ?)}";
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setLong(StoredProcedureParameterName.USER_ID, userId);
			callableStatement.setInt(StoredProcedureParameterName.USER_ACTIVE, active);
			
			LOGGER.trace(sqlQuery);
			
			if (callableStatement.executeUpdate() != 1) { 
				throw new SQLException(
					SqlExceptionMessage.USER_HAS_NOT_BEEN_DELETED
				);
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.USER_ACTIVITY_STATE_NOT_CHANGED, e);
			
			throw new SQLException(
				SqlExceptionMessage.USER_ACTIVITY_STATE_NOT_CHANGED, e
			);
		}
	}

	/**
	 * Get users by role as Java Stream.
	 * 
	 * 
	 * @return a lazily populated stream of User objects by role. 
	 * 		Note the stream returned must be closed to free all the acquired 
	 * 		resources.
	 * 		The stream keeps an open connection to the database till it is 
	 * 		complete or is closed manually.
	 */
	@Override
	public Stream<User> getUsersByRole(Role role) throws SQLException {
		final String sqlQuery = "{call get_users_by_role(?)}";
		
		try  {
			Connection connection = dataSourceManagement.getConnection();
			CallableStatement callableStatement = connection.prepareCall(sqlQuery);
			
			callableStatement.setString(
				StoredProcedureParameterName.USER_ROLE,
				role.name()
			);
			
			ResultSet resultSet = callableStatement.executeQuery(); 
	
			LOGGER.trace(sqlQuery);
			
			return Util.<User>createStreamOf(
					connection,
					callableStatement,
					resultSet,
					userMapper::mapRow,
					LOGGER
			);
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_THE_USER_OBJECTS, e);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_THE_USER_OBJECTS, e
			);
		}
	}

	@Override
	public Optional<User> getUserByLogin(String login) throws SQLException {
		final String sqlQuery = "{call get_user_by_login(?)}";
		
		Optional<User> user = Optional.empty();
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setString(StoredProcedureParameterName.USER_LOGIN, login);
			
			try (ResultSet resultSet = callableStatement.executeQuery()) {
				if (resultSet.next()) {
					user = Optional.ofNullable(userMapper.mapRow(resultSet));
				}
			}
			
			LOGGER.trace(sqlQuery);
			
			return user;
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_USER_BY_LOGIN, e);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_USER_BY_LOGIN, e
			);
		}
	}


	@Override
	public Optional<User> getUserByEmail(String email) throws SQLException {
		final String sqlQuery = "{call get_user_by_email(?)}";
		
		Optional<User> user = Optional.empty();
		
		try (
				Connection connection = dataSourceManagement.getConnection();
				CallableStatement callableStatement = connection.prepareCall(sqlQuery)
		) {
			callableStatement.setString(
				StoredProcedureParameterName.USER_EMAIL, email
			);
			
			try (ResultSet resultSet = callableStatement.executeQuery()) {
				if (resultSet.next()) {
					user = Optional.ofNullable(userMapper.mapRow(resultSet));
				}
			}
			
			LOGGER.trace(sqlQuery);
			
			return user;
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_USER_BY_EMAIL, e);
			
			throw new SQLException(
				SqlExceptionMessage.CANNOT_GET_USER_BY_EMAIL, e
			);
		}
	}
}
