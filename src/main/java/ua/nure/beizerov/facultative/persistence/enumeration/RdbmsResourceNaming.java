package ua.nure.beizerov.facultative.persistence.enumeration;


public enum RdbmsResourceNaming {
	
	MY_SQL("jdbc/facultativeDB"),
	MY_SQL_TEST("TEST");
	
	
	final String dbNaming;
	
	
	private RdbmsResourceNaming(String dbNaming) {
		this.dbNaming = dbNaming;
	}


	public String getDbNaming() {
		return dbNaming;
	}
}
