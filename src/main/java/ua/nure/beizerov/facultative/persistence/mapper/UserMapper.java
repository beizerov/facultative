package ua.nure.beizerov.facultative.persistence.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.beizerov.facultative.model.Role;
import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;


/**
 * Extracts a user from the result set row.
 */
public class UserMapper implements EntityMapper<User> {

	@Override
	public User mapRow(ResultSet resultSet) throws SQLException {
        User user = new User();
        
        user.setId(resultSet.getLong(
        	StoredProcedureParameterName.USER_ID)
        );
        user.setActive(
        	resultSet.getInt(StoredProcedureParameterName.USER_ACTIVE)
        );
        user.setEmail(
        	resultSet.getString(StoredProcedureParameterName.USER_EMAIL)
        );
        user.setLogin(
        	resultSet.getString(StoredProcedureParameterName.USER_LOGIN)
        );
        user.setFirstName(
        	resultSet.getString(
        		StoredProcedureParameterName.USER_FIRST_NAME
        	)
        );
        user.setLastName(
        	resultSet.getString(StoredProcedureParameterName.USER_LAST_NAME)
        );
        user.setPassword(
        	resultSet.getString(StoredProcedureParameterName.USER_PASSWORD)
        );
        user.setRole(
        	Role.valueOf(
        		resultSet.getString(StoredProcedureParameterName.USER_ROLE)
        	)
        );
        user.setLocale(
        	resultSet.getString(StoredProcedureParameterName.USER_LOCALE)
        );
        user.setPathToPhoto(
        	resultSet.getString(
        		StoredProcedureParameterName.USER_PATH_TO_PHOTO
        	)
        );

        return user;
	}
}