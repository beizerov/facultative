package ua.nure.beizerov.facultative.persistence.enumeration;


/**
 * This enumeration represents the sort order 
 * in ascending and descending order in an SQL query.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public enum Order {
	ASC,
	DESC
}
