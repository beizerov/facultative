package ua.nure.beizerov.facultative.persistence.dao;


import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

import ua.nure.beizerov.facultative.model.Role;
import ua.nure.beizerov.facultative.model.User;


/**
 * Defines a contract for classes that implement this interface. 
 * UserDAO is a data access interface for implementations of 
 * various storage systems.
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @see Dao
 */
public interface UserDao extends Dao<User> {
	
	/**
	 * Creates a data record in the storage system 
	 * and registers the user teacher for the selected course.
	 * 
	 * 
	 * @param teacher is a user with teacher role ({@link Role#TEACHER})
	 * @param courseId course id
 	 * @throws SQLException if any SQL error occurs
	 */
	void createTeacherForSelectedCourse(User teacher, long courseId)
			throws SQLException;
	
	/**
	 * Set user activity.
	 * 
	 * 
	 * @param userId user id
	 * @param active 0 or 1 like false and true
	 * @throws SQLException if any SQL error occurs
	 */
	void setActive(long userId, int active) throws SQLException;
	
	/**
	 * Get users by role.
	 * 
	 * 
	 * @param role any of the user roles from {@link Role}
	 * @return a lazily populated stream of users by role.
	 * 		Note the stream returned must be closed to free all the acquired 
	 * 		resources. 
	 * 		The stream keeps an open connection to the database 
	 * 		till it is complete or is closed manually.
	 * @throws SQLException if any SQL error occurs
	 */
	Stream<User> getUsersByRole(Role role) throws SQLException;
	
	
	/**
	 * Get user by login.
	 * 
	 * 
	 * @param login user login
	 * @return an optional with user if a user with <code>login</code> 
	 * 		exists, empty optional otherwise
	 * @throws SQLException if any SQL error occurs
	 */
	Optional<User> getUserByLogin(String login) throws SQLException;
	
	/**
	 * Get user by email.
	 * 
	 * 
	 * @param email user email
	 * @return an optional with user if a user with <code>email</code> 
	 * 		exists, empty optional otherwise
	 * @throws SQLException if any SQL error occurs
	 */
	Optional<User> getUserByEmail(String email) throws SQLException;
}
