package ua.nure.beizerov.facultative.persistence;


import java.io.Serializable;

import ua.nure.beizerov.facultative.model.Entity;
import ua.nure.beizerov.facultative.persistence.dto.Dto;


/**
 * Superinterface for {@link Entity} and {@link Dto}.
 * It is an abstraction.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public interface Data extends Serializable {

}
