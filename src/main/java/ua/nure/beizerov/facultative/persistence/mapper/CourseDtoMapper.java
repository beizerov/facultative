package ua.nure.beizerov.facultative.persistence.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;
import ua.nure.beizerov.facultative.persistence.dto.CourseDto;


/**
 * Extracts a course and other info from view veiw_courses
 * from the result set row.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class CourseDtoMapper 
		implements DtoMapper<CourseDto> {

	@Override
	public CourseDto mapRow(ResultSet resultSet) 
			throws SQLException {
		CourseDto courseDto = new CourseDto();
		
		courseDto.setCourseId(
			resultSet.getLong(StoredProcedureParameterName.COURSE_ID)
		);
		courseDto.setCourseName(
			resultSet.getString(StoredProcedureParameterName.COURSE_NAME)
		);
		courseDto.setStartDate(
			resultSet.getDate(
				StoredProcedureParameterName.COURSE_START_DATE
			).toLocalDate()
		);
		courseDto.setEndDate(
			resultSet.getDate(
				StoredProcedureParameterName.COURSE_END_DATE
			).toLocalDate()
		);
		courseDto.setDuration(
			resultSet.getInt(StoredProcedureParameterName.COURSE_DURATION)
		);
		courseDto.setNumberOfstudents(
			resultSet.getLong(
				StoredProcedureParameterName.COURSE_NUMBER_OF_STUDENTS
			)
		);
		courseDto.setDescription(
			resultSet.getString(StoredProcedureParameterName.COURSE_DESCRIPTION)
		);
		courseDto.setSubjectName(
			resultSet.getString(
				StoredProcedureParameterName.SUBJECT_NAME
			)
		);
		courseDto.setTeacherFullName(
			resultSet.getString(
				StoredProcedureParameterName.COURSE_TEACHER_FULL_NAME
			)
		);
		
		return courseDto;
	}
}
