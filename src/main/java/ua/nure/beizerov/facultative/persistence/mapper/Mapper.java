package ua.nure.beizerov.facultative.persistence.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Superinterface for {@link EntityMapper} and {@link DtoMapper}
 * for mapping of objects.
 * 
 * <p>
 * 	Implementations are not supposed to move cursor of the resultSet via next() 
 * 	method, but only extract information from the row 
 * 	in current cursor position.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <E> Type for mapping class.
 */
public interface Mapper<E> {
    E mapRow(ResultSet resultSet) throws SQLException;
}
