package ua.nure.beizerov.facultative.persistence.mapper;


import ua.nure.beizerov.facultative.model.Entity;


/**
 * <p>
 * 	Defines general contract for mapping database result set rows 
 * 	to application entities.
 * </p>
 * <p>
 * 	Implementations are not supposed to move cursor of the resultSet via next() 
 * 	method, but only extract information from the row 
 * 	in current cursor position.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <E> All subclasses of {@link Entity} class.
 * 
 * @see Mapper
 */
public interface EntityMapper<E extends Entity>  extends Mapper<E>{
	
}
