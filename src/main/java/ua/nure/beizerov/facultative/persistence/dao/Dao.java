package ua.nure.beizerov.facultative.persistence.dao;


import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

import ua.nure.beizerov.facultative.model.Entity;


/**
 * <p>
 * 	Defines a common contract for DAO classes that implement this interface. 
 * 	Implementing classes must implement CRUD methods.
 * </p>
 * 
 * <p>
 * 	GenericDao and the interfaces that extend it separate
 * 	low-level data access logic from business logic.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <E> E extends {@link Entity}
 * 
 */
public interface Dao<E extends Entity> {
	
	/**
	 * Create an entry in persistence system.
	 * 
	 * 
	 * @param entity an object of a class that extends the class Entity
	 * @throws SQLException if any SQL error occurs
	 * @throws UnsupportedOperationException if not overridden
	 */
	default void create(E entity) throws SQLException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Delete an entry in persistence system.
	 * 
	 * 
	 * @param id entity id
	 * @throws SQLException if any SQL error occurs
	 * @throws UnsupportedOperationException if not overridden 
	 */
	default void delete(long id) throws SQLException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Update an entry in persistence system by entity.
	 * 
	 * 
	 * @param an object of a class that extends the class Entity
	 * @throws SQLException if any SQL error occurs
	 * @throws UnsupportedOperationException if not overridden
	 */
	default void update(E entity) throws SQLException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Get entry by id from persistence system as entity.
	 * 
	 * 
	 * @param id 
	 * @return an optional with entity if an entity with unique identifier 
	 * 		<code>id</code> exists, empty optional otherwise
	 * @throws SQLException if any SQL error occurs
	 * @throws UnsupportedOperationException if not overridden
	 */
	default Optional<E> getById(long id) throws SQLException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Get all entries as entities.
	 * 
	 * 
	 * @return the entities as a stream. 
	 * 		The stream may be lazily or eagerly evaluated based on 
	 * 		the implementation. The stream must be closed after use
	 * @throws SQLException	if any SQL error occurs
	 * @throws UnsupportedOperationException if not overridden
	 */
	default Stream<E> getAll() throws SQLException {
		throw new UnsupportedOperationException();
	}
}
