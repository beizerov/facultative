package ua.nure.beizerov.facultative.persistence.constant;


/**
 * Contains table column name constants.
 * 
 * @author Oleksii Beizerov
 *
 */
public final class StoredProcedureParameterName {

	private StoredProcedureParameterName() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	/*
	 * See src/main/resources/sql/db-create-mysql.sql
	 */
	
	// courses. Also present in view_courses
	public static final String COURSE_ID = "course_id";
	public static final String COURSE_NAME = "course_name";
	public static final String COURSE_START_DATE = "start_date";
	public static final String COURSE_END_DATE = "end_date";
	public static final String COURSE_DESCRIPTION = "description";
	
	// OUT parameter for stored procedure 
	//	save_course_for_selected_subject(?, ?, ?, ?, ?, ?)
	public static final String OUT_PARAM_IS_OK = "isOk";
	
	// view_courses
	public static final String COURSE_DURATION = "duration";
	public static final String COURSE_NUMBER_OF_STUDENTS = "number_of_students";
	public static final String COURSE_TEACHER_FULL_NAME = "teacher_full_name";

	// gradebook
	public static final String GRADE = "grade";
	
	// sort order is a parameter for stored procedures
	public static final String SORT_ORDER = "sort_order";
	
	// students_courses
	public static final String STUDENT_ID = "student_id";
	
	// subjects. subject_id and subject_name also present in view_courses
	public static final String SUBJECT_ID = "subject_id";
	public static final String SUBJECT_NAME = "subject_name";
	public static final String SUBJECT_DESCRIPTION = "description";
	
	// teachers_courses.  Also present in view_courses
	public static final String TEACHER_ID = "teacher_id";
	
	// users
	public static final String USER_ID = "user_id";
	public static final String USER_ACTIVE = "active";
	public static final String USER_EMAIL = "email";
	public static final String USER_LOGIN = "login";
	public static final String USER_FIRST_NAME = "first_name";
	public static final String USER_LAST_NAME = "last_name";
	public static final String USER_PASSWORD = "password";
	public static final String USER_ROLE = "role";
	public static final String USER_LOCALE = "locale_language";
	public static final String USER_PATH_TO_PHOTO = "path_to_photo";
}
