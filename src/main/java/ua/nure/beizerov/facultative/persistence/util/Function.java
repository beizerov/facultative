package ua.nure.beizerov.facultative.persistence.util;


import java.sql.SQLException;


/**
 * Represents a function that accepts one argument and produces a result. 
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <T> the type of the input to the function
 * @param <R> the type of the result of the function
 */
@FunctionalInterface
public interface Function<T, R> {
	
	/**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
	 * @throws SQLException if any SQL error occurs
	 */
	R apply(T t) throws SQLException;
}
