package ua.nure.beizerov.facultative.persistence.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.beizerov.facultative.model.Subject;
import ua.nure.beizerov.facultative.persistence.constant.StoredProcedureParameterName;


/**
 *  Extracts a subject from the result set row.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class SubjectMapper implements EntityMapper<Subject> {

	@Override
	public Subject mapRow(ResultSet resultSet) throws SQLException {
		Subject subject = new Subject();
		
		subject.setId(
			resultSet.getLong(StoredProcedureParameterName.SUBJECT_ID)
		);
		subject.setName(
			resultSet.getString(StoredProcedureParameterName.SUBJECT_NAME)
		);
		
		return subject;
	}
}
