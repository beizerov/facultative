package ua.nure.beizerov.facultative.persistence.dao;


import ua.nure.beizerov.facultative.model.Subject;


/**
 * Defines a contract for classes that implement this interface. 
 * SubjectDAO is a data access interface for implementations of 
 * various storage systems.
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @see Dao
 */
public interface SubjectDao extends Dao<Subject> {

}
