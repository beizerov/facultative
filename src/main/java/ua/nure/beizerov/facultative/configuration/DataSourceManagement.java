package ua.nure.beizerov.facultative.configuration;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import ua.nure.beizerov.facultative.persistence.enumeration.RdbmsResourceNaming;


public class DataSourceManagement {
	
	private static final Logger LOGGER = Logger.getLogger(DataSourceManagement.class);
	
	private DataSource dataSource;
	
	
	public DataSourceManagement(RdbmsResourceNaming rdbmsResourceNaming) {
		setDataSource(rdbmsResourceNaming);
	}
	
	
	private void setDataSource(
		RdbmsResourceNaming rdbmsResourceNaming
	) {
		switch (rdbmsResourceNaming) {
			case MY_SQL:
				Context initialContext;
				
				try {
					initialContext = new InitialContext();
				
					dataSource = (DataSource) initialContext.lookup(
						rdbmsResourceNaming.getDbNaming()
					);
				} catch (NamingException e) {
					LOGGER.error(e);
				}
			
				break;
			case MY_SQL_TEST:
				// For JUnit testing
				HikariConfig hikariConfig = new HikariConfig(
					"src/test/resources/application.properties"
				);
				
				hikariConfig.setTransactionIsolation(
					"TRANSACTION_READ_COMMITTED"
				);
				
				HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig);
				
				dataSource = hikariDataSource;

				break;
			default:
				break;
		}
	}
	
	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
}
