package ua.nure.beizerov.facultative.configuration;


import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


/**
 * This class is a MySQL pool configuration. For create a connection pool, used
 * {@link BasicDataSource}
 * 
 * @author Oleksii Beizerov
 *
 */
public final class MySqlDbcpDataSourceConfiguration {

	private static final Logger LOGGER = Logger.getLogger(
			MySqlDbcpDataSourceConfiguration.class
	);
	
	private static HikariDataSource hikariDataSource;
	
	
	private MySqlDbcpDataSourceConfiguration() {
		throw new IllegalStateException("Do not create instances!");
	}

	
	/**
	 * @return {@link BasicDataSource}
	 * @throws ApplicationException
	 */
	public static synchronized void initDataSource() {
		if (hikariDataSource == null) {
			HikariConfig hikariConfig = new HikariConfig();
			
			Map<String, String> environmentVariables = System.getenv();

			hikariConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");
			hikariConfig.setJdbcUrl(
				"jdbc:mysql://" 
				+ environmentVariables.get("MYSQL_DB_HOST") 
				+ "/" 
				+ environmentVariables.get("MYSQL_DB_NAME") 
				+ "?useSSL=true"
			);
			hikariConfig.setUsername(
				environmentVariables.get("MYSQL_DB_USERNAME")
			);
			hikariConfig.setPassword(
				environmentVariables.get("MYSQL_DB_PASSWORD")
			);
			hikariConfig.setTransactionIsolation(
				"TRANSACTION_READ_COMMITTED"
			);
			hikariConfig.setMinimumIdle(
				Integer.parseInt(environmentVariables.get("MYSQL_CP_MIN_IDLE"))
			);
			hikariConfig.setMaximumPoolSize(
				Integer.parseInt(environmentVariables.get(
					"MYSQL_CP_MAX_POOL_SIZE")
				)
			);
			
			hikariDataSource = new HikariDataSource(hikariConfig);
			
			try {
				System.setProperty(
					Context.INITIAL_CONTEXT_FACTORY,
					"org.apache.naming.java.javaURLContextFactory"
				);
				
				InitialContext initialContext = new InitialContext();
				
				initialContext.createSubcontext("jdbc");
				
				initialContext.bind(
					"jdbc/facultativeDB", hikariDataSource
				);
			} catch (NamingException e) {
				LOGGER.error(e);
			}
		}		
	}
}
