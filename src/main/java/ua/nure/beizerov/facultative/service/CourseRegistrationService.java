package ua.nure.beizerov.facultative.service;


import java.sql.SQLException;
import java.time.LocalDate;
import javax.servlet.http.HttpServletRequest;

import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.persistence.dao.CourseDao;
import ua.nure.beizerov.facultative.service.validation.CourseRegistrationValidationService;


public class CourseRegistrationService {
	
	private final CourseDao courseDao;
	private final CourseRegistrationValidationService courseValidationService;
	
	
	/**
	 * @param courseDao a class that implements {@link CourseDao}
	 * @throws IllegalArgumentException if in courseDao pass null
	 */
	public CourseRegistrationService(CourseDao courseDao) {
		if (courseDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.courseDao = courseDao;
		this.courseValidationService = new CourseRegistrationValidationService(courseDao);
	}
	
	
	public boolean registerCourse(HttpServletRequest httpServletRequest) 
			throws SQLException {
		
		String subjectSelection = httpServletRequest.getParameter("subjectSelection");
		
		if (
			subjectSelection == null 
			|| 
			subjectSelection.isBlank() 
			|| 
			!subjectSelection.matches("^\\d+$")
		) {
			throw new IllegalArgumentException(
				"subjectSelection must be an integer only."
			);
		}
		
		Course course = fillCourse(httpServletRequest);
		
		if (courseValidationService.isNotPresent(course, httpServletRequest)) {
			int subjectId = Integer.parseInt(subjectSelection);
			
			courseDao.createForSelectedSubject(course, subjectId);
			
			return true;
		}
		
		return false;
	}
	
	
	private static Course fillCourse(
			HttpServletRequest httpServletRequest
	) {
		Course course = new Course();
		
		course.setName(httpServletRequest.getParameter("courseName"));
		course.setStartDate(LocalDate.parse(
			httpServletRequest.getParameter("startDate")
		));
		course.setEndDate(LocalDate.parse(
			httpServletRequest.getParameter("endDate")
		));
		course.setDescription(
			httpServletRequest.getParameter("courseDescription")
		);
		
		return course;
	}
}