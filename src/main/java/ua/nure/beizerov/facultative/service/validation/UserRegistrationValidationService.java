package ua.nure.beizerov.facultative.service.validation;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.dao.UserDao;
import ua.nure.beizerov.facultative.persistence.exception.constant.SqlExceptionMessage;


/**
 * @author Oleksii Beizerov
 *
 */
public class UserRegistrationValidationService {

	private static final Logger LOGGER = Logger.getLogger(UserRegistrationValidationService.class);
	
	private static final String LOGIN_PATTERN = "^(?=[\\p{IsLatin}\\p{Nd}_!@])(?:[\\p{IsLatin}\\.\\p{Nd}_!@](?<!\\.{2})){2,25}(?<!\\.)$";
	// TODO EMAIL_PATTERN !!! INVALID !!!
	private static final String EMAIL_PATTERN = "^(?:[\\p{IsLatin}\\p{Nd}](\\.(?=[\\p{IsLatin}\\p{Nd}]))?[\\p{IsLatin}\\p{Nd}]?){6,15}@[a-z]+\\.[a-z]{2,}$";
	private static final String NAME_PATTERN = "^\\p{Lu}\\p{Ll}{2,12}$";
	
	private UserDao userDao;
	
	
	public UserRegistrationValidationService(UserDao userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.userDao = userDao;
	}
	
	
	/**
	 * Verify that the user is not present in the data storage system.
	 * 
	 * 
	 * @param user user
	 * @param httpServletRequest is instance HttpServletRequest 
	 * @return true if user is not present
	 */
	public boolean isNotPresent(
			User user, HttpServletRequest httpServletRequest
	) {
		if (user == null) {
			return false;
		}
		
		boolean isntPresent = true;
		
		Map<String, String> messages = new HashMap<>();
		httpServletRequest.setAttribute("messages", messages);
		
		try {
			if (userDao.getUserByEmail(user.getEmail()).isPresent()) {
				messages.put(
					"emailError",
					"The user with this email is already registered"
				);
				
				LOGGER.debug("The user with this email is already registered");
				
				isntPresent = false;
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_USER_BY_EMAIL, e);
		}
		
		try {
			if (userDao.getUserByLogin(user.getLogin()).isPresent()) {
				messages.put(
					"loginError",
					"The user with this login is already registered"
				);
				
				LOGGER.debug("The user with this login is already registered");
				
				isntPresent = false;
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_USER_BY_LOGIN, e);
		}
		
		return isntPresent;
	}
	
	public boolean validateUserData(
			User user, HttpServletRequest httpServletRequest
	) {
		Map<String, String> messages = new HashMap<>();
		httpServletRequest.setAttribute("messages", messages);
		boolean isValid = true;
		
		if (!user.getEmail().matches(EMAIL_PATTERN)) {
			messages.put(
				"emailError",
					
				"Sorry email should consist only of letters [A-Za-z],"
				+ " numbers [0-9], and periods[.] are allowed. "
				+ "The period character cannot be at the beginning or at the end."
				+ " In addition, there cannot be a period character after period character"
				+ "Example: username@email.com"
			);
			
			LOGGER.debug("Invalid email");
			
			isValid = false;
		} 
		
		if (!user.getLogin().matches(LOGIN_PATTERN)) {
			messages.put(
				"loginError",
				
				"Sorry, login must be between 6 and 30 characters long."
				+ " Letters, numbers and period."
				+ " A period character cannot be first or last."
			);
			
			LOGGER.debug("Invalid login");
			
			isValid = false;
		} 
		
		if (!user.getFirstName().matches(NAME_PATTERN)) {
			messages.put(
				"firstNameError",
				"The first letter is uppercase. Minimum 3 letters. Example: Sam"
			);
			
			LOGGER.debug("Invalid first name");
			
			isValid = false;
		}
		
		if (!user.getLastName().matches(NAME_PATTERN)) {
			messages.put(
				"lastNameError",
				"The first letter is uppercase. Minimum 3 letters. Example: Sam"
			);
			
			LOGGER.debug("Invalid last name");
			
			isValid = false;
		}
		
		return isValid;
	}
}
