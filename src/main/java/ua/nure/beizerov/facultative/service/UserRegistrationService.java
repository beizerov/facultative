package ua.nure.beizerov.facultative.service;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.Role;
import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.dao.UserDao;
import ua.nure.beizerov.facultative.service.validation.UserRegistrationValidationService;


public class UserRegistrationService {
	
	private static final Logger LOGGER = Logger.getLogger(UserRegistrationService.class);
	private static final String UPLOAD_DIRECTORY = "photo";
	
	private final UserDao userDao;
	private final UserRegistrationValidationService userValidationService;
	
	
	/**
	 * @param userDao a class that implements {@link UserDao}
	 * @throws IllegalArgumentException if in courseDao pass null
	 */
	public UserRegistrationService(UserDao userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.userDao = userDao;
		this.userValidationService = new UserRegistrationValidationService(userDao);
	}
	
	
	public boolean registerStudent(HttpServletRequest httpServletRequest)
			throws SQLException, IOException, ServletException {
		User user = fillUser(httpServletRequest, Role.STUDENT);
		
		if (
			userValidationService.isNotPresent(user, httpServletRequest)
			&&
			userValidationService.validateUserData(user, httpServletRequest)
		) {
			user.setPathToPhoto(saveUserPhoto(httpServletRequest));
			
			userDao.create(user);
			
			return true;
		}
		
		return false;
	}
	
	public boolean registerTeacher(HttpServletRequest httpServletRequest)
			throws  SQLException, IOException, ServletException {
		String courseSelection = httpServletRequest.getParameter("courseSelection");
		
		if (
			courseSelection == null 
			|| 
			courseSelection.isBlank() 
			|| 
			!courseSelection.matches("^\\d+$")
		) {
			throw new IllegalArgumentException(
				"courseSelection must be an integer only."
			);
		}
		
		User user = fillUser(httpServletRequest, Role.TEACHER);
		
		if (
			userValidationService.isNotPresent(user, httpServletRequest)
			&&
			userValidationService.validateUserData(user, httpServletRequest)
		) {
			int courseId = Integer.parseInt(courseSelection);
			
			user.setPathToPhoto(saveUserPhoto(httpServletRequest));
			
			userDao.createTeacherForSelectedCourse(user, courseId);
			
			return true;
		}
		
		return false;
	}
	
	
	private static User fillUser(
			HttpServletRequest httpServletRequest, Role role
	) {
		User user = new User();

		user.setRole(role);
		user.setActive(1);
		user.setEmail(httpServletRequest.getParameter("email"));
		user.setLogin(httpServletRequest.getParameter("login"));
		user.setFirstName(httpServletRequest.getParameter("firstName"));
		user.setLastName(httpServletRequest.getParameter("lastName"));
		user.setPassword(httpServletRequest.getParameter("password"));
		
		return user;
	}

	/**
	 * The method receives an http request and returns the path to the photo.
	 * 
	 * 
	 * @param httpServletRequest http request
	 * @return String path to photo
	 * @throws ServletException 
	 * @throws IOException 
	 */
	private static String saveUserPhoto(
			HttpServletRequest httpServletRequest
	) throws IOException, ServletException {
		if (isPresentFile(httpServletRequest)) {
			LOGGER.debug("Photo is present");
			
			Path uploadPath = Path.of( 
					httpServletRequest
						.getServletContext()
						.getRealPath(""),
					UPLOAD_DIRECTORY
			);
			
			File uploadDir = uploadPath.toFile();
			if (Files.notExists(uploadPath)) {
				uploadDir.mkdir();
			}
			
			StringBuilder stringBuilder = new StringBuilder();
			
			for (Part part : httpServletRequest.getParts()) {
				if ("fileName".equals(part.getName())) {
					stringBuilder
						.append(UUID.randomUUID())
						.append(part.getSubmittedFileName())
					;
					
					part.write(
							uploadPath 
							+ File.separator
							+ stringBuilder.toString()
					);
				}
			}
			
			LOGGER.debug(stringBuilder);
			
			return stringBuilder.toString();
		} else {
			LOGGER.debug("Photo not present");
			
			return null;
		}
	}
	
	private static boolean isPresentFile(HttpServletRequest httpServletRequest)
			throws IOException, ServletException {
		return httpServletRequest.getPart("fileName").getSize() > 0;
	}
}
