package ua.nure.beizerov.facultative.service;


import java.sql.SQLException;
import java.time.LocalDate;
import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.persistence.dao.CourseDao;


public class EnrollService {

	private static final Logger LOGGER = Logger.getLogger(EnrollService.class);
	
	private CourseDao courseDao;
	
	
	/**
	 * @param courseDao a class that implements {@link CourseDao}
	 * @throws IllegalArgumentException if in courseDao pass null
	 */
	public EnrollService(CourseDao courseDao) {
		if (courseDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.courseDao = courseDao;
	}
	
	
	public void enrollStudent(long studentId, long courseId) 
			throws SQLException {
		Course course = courseDao.getById(courseId).orElseThrow();
			
		if(course.getStartDate().isAfter(LocalDate.now())) {
			courseDao.enrollStudent(studentId, courseId);
			
			LOGGER.info("In a course: " + course.getName());
		}
	}
}
