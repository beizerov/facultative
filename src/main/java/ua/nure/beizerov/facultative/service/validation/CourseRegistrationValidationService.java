package ua.nure.beizerov.facultative.service.validation;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.persistence.dao.CourseDao;


public class CourseRegistrationValidationService {

	private static final Logger LOGGER = Logger.getLogger(CourseRegistrationValidationService.class);
	
	private CourseDao courseDao;
	
	
	public CourseRegistrationValidationService(CourseDao courseDao) {
		if (courseDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.courseDao = courseDao;
	}
	
	
	/**
	 * Verify that the course is not present in the data storage system.
	 * 
	 * 
	 * @param course course
	 * @param httpServletRequest is instance HttpServletRequest 
	 * @return true if course is not present
	 */
	public boolean isNotPresent(
			Course course, HttpServletRequest httpServletRequest
	) {
		if (course == null) {
			return false;
		}
		
		boolean isntPresent = true;
		
		try {
			if(courseDao.getByName(course.getName()).isPresent()) {
				Map<String, String> messages = new HashMap<>();
				httpServletRequest.setAttribute("messages", messages);
				
				messages.put(
					"courseNameError",
					"The course with this name is already registered"
				);
					
				LOGGER.debug("The course with this name is already registered");
				
				isntPresent = false;
			}
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		
		return isntPresent;
	}
	
	public boolean validateCourseData(
			Course course, int subjectId, HttpServletRequest httpServletRequest
	) {
		
		return false;
	}
}
