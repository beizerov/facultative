package ua.nure.beizerov.facultative.service.util;


import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import ua.nure.beizerov.facultative.model.Role;
import ua.nure.beizerov.facultative.model.User;


/**
 * @author Oleksii Beizerov
 *
 */
public final class AuthorizationService {
	
	private static Map<Role, List<String>> availablePagesByRole;
	
	public static final String ERROR_PAGE = "/error";
	
	
	private AuthorizationService() {
		throw new IllegalStateException("Util class");
	}
	
	
	static {
		availablePagesByRole = new EnumMap<>(Role.class);
		
		availablePagesByRole.put(
				Role.ADMIN, 
				Stream.of(
					"teacherRegistration",
					"courseRegistration",
					"subjectRegistration"
				).collect(Collectors.toCollection(ArrayList::new))
		);
	}
	

	public static String getUserAuthorizationTokenUri(User user) {
		switch (user.getRole()) {
		case ADMIN:
			return "/admin/";
		case TEACHER:
			return "/teacher/";
		case STUDENT:
			return "/student/";
		default:
			return ERROR_PAGE;	
		}
	}

	
	public static boolean isOwner(
			HttpServletRequest httpRequest, User authUser
	) {
		String requestUri = httpRequest.getRequestURI();
		String userHomePageUri = 
				httpRequest.getContextPath()
				+ AuthorizationService.getUserAuthorizationTokenUri(authUser) 
				+ authUser.getLogin() 
		; 
		
		return 
				requestUri.equals(userHomePageUri) 
				|| 
				isAvailableUri(httpRequest, requestUri, authUser)
		;
	}
	
	
	private static boolean isAvailableUri(
			HttpServletRequest httpRequest, String uri, User user
	) {
		for (String availableUri : availablePagesByRole.get(user.getRole())) {
			if ((
					httpRequest.getContextPath()
					+ getUserAuthorizationTokenUri(user)
					+ availableUri).equals(uri)
			) {
				return true;
			}
		} 
		
		return false;
	}
}
