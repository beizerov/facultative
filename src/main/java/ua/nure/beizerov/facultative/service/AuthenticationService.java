package ua.nure.beizerov.facultative.service;


import java.sql.SQLException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.facultative.model.User;
import ua.nure.beizerov.facultative.persistence.dao.UserDao;
import ua.nure.beizerov.facultative.persistence.exception.constant.SqlExceptionMessage;


/**
 * @author Oleksii Beizerov
 *
 */
public class AuthenticationService {
	
	private static final Logger LOGGER = Logger.getLogger(AuthenticationService.class);
	
	private final UserDao userDao;
	
	
	/**
	 * @param userDao a class that implements {@link UserDao}
	 * @throws IllegalArgumentException if in courseDao pass null
	 */
	public AuthenticationService(UserDao userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.userDao = userDao;
	}
	
	
	public Optional<User> authentication(HttpServletRequest request) {
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		
		if (login == null || password == null) {
			return Optional.empty();
		}
		
		try {
			Optional<User> user = userDao.getUserByLogin(login);
			
			if (user.isPresent() && user.get().getPassword().equals(password)) {
				return user;
			}
		} catch (SQLException e) {
			LOGGER.error(SqlExceptionMessage.CANNOT_GET_USER_BY_LOGIN, e);
		}

		return Optional.empty();
	}
	
	
	public static Optional<User> getAuthenticatedUser(
			HttpSession currentSession
	) {
		return Optional.ofNullable((User) currentSession.getAttribute("user"));
	}
}
