package ua.nure.beizerov.facultative.service.util;


import javax.servlet.http.HttpServletRequest;


public final class MessageService {
	
	private MessageService() {
		throw new IllegalStateException("Utility class");
	}
	
	public static void addMessage(
			HttpServletRequest httpServletRequest,
			String messageName,
			String message
	) {
		httpServletRequest.setAttribute(messageName, message);
	}
}
