package ua.nure.beizerov.facultative.service;


import java.sql.SQLException;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ua.nure.beizerov.facultative.model.Course;
import ua.nure.beizerov.facultative.persistence.dao.CourseDao;
import ua.nure.beizerov.facultative.persistence.dto.CourseDto;
import ua.nure.beizerov.facultative.persistence.enumeration.Order;


/**
 * @author Oleksii Beizerov
 *
 */
public class GettingCourseService {

	private CourseDao courseDao;
	
	
	/**
	 * @param courseDao a class that implements {@link CourseDao}
	 * @throws IllegalArgumentException if in courseDao pass null
	 */
	public GettingCourseService(CourseDao courseDao) {
		if (courseDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.courseDao = courseDao;
	}
	
	
	/**
	 * Get all CourseDto objects.
	 * 
	 * 
	 * @param key column name or key in NoSQL of according to which 
	 * 		the data is needed to be arranged
	 * @param order the sort order
	 * 		for sorting the returned data
     * @param collectionFactory a supplier providing a new empty {@code Collection}
     *                          into which the results will be inserted
	 * @return all the CourseDto objects as a stream. The stream may be lazily 
	 * 		or eagerly evaluated based on the implementation. 
	 * 		The stream must be closed after use.
	 * @throws SqlApplicationException if any SQL error occurs
	 */
	public Collection<CourseDto> getAll(
			String key,
			Order order,
			Supplier<? extends Collection<CourseDto>>  collectionFactory 
	) throws SQLException {
		try (Stream<CourseDto> streamOfCourses = courseDao.getAll(key, order)) {
			return streamOfCourses.collect(
				Collectors.toCollection(collectionFactory)
			);
		}
	}
	
	/**
	 * Get all courses.
	 * 
	 * 
     * @param collectionFactory a supplier providing a new empty {@code Collection}
     *                          into which the results will be inserted
	 * @return all the Course objects as a stream. The stream may be lazily 
	 * 		or eagerly evaluated based on the implementation. 
	 * 		The stream must be closed after use.
	 * @throws SqlApplicationException if any SQL error occurs
	 */
	public Collection<Course> getAll(
			Supplier<? extends Collection<Course>>  collectionFactory 
	) throws SQLException {
		try (Stream<Course> streamOfCourses = courseDao.getAll()) {
			return streamOfCourses.collect(
				Collectors.toCollection(collectionFactory)
			);
		}
	}
}
