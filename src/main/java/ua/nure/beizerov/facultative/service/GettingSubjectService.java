package ua.nure.beizerov.facultative.service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ua.nure.beizerov.facultative.model.Subject;
import ua.nure.beizerov.facultative.persistence.dao.SubjectDao;


public class GettingSubjectService {
	
	private SubjectDao subjectDao;
	

	/**
	 * @param subjectDao a class that implements {@link SubjectDao}
	 * @throws IllegalArgumentException if in courseDao pass null
	 */
	public GettingSubjectService(SubjectDao subjectDao) {
		if (subjectDao == null) {
			throw new IllegalArgumentException("The argument cannot be null.");
		}
		
		this.subjectDao = subjectDao;
	}
	
	
	/**
	 * Get collection of all subjects.
	 * 
	 * 
	 * @param collectionFactory a supplier providing a new empty {@code Collection}
     *        		into which the results will be inserted
	 * @return collection of subjects
	 * @throws SQLException if any SQL error occurs
	 */
	public Collection<Subject> getAll(
			Supplier<? extends Collection<Subject>>  collectionFactory
	) throws SQLException {
		try (Stream<Subject> streamOfSubjects = subjectDao.getAll()) {
			return streamOfSubjects.collect(
				Collectors.toCollection(collectionFactory)
			);
		}
	}
}
