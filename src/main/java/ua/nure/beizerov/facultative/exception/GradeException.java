package ua.nure.beizerov.facultative.exception;


/**
 * @author Oleksii Beizerov
 *
 */
public class GradeException extends ApplicationException {

	private static final long serialVersionUID = 8594674698362973177L;

	
	public GradeException(String message) {
		super(message);
	}
}
