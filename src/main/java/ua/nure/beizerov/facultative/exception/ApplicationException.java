package ua.nure.beizerov.facultative.exception;


/**
 * An exception that provides information on an application error.
 * 
 * @author Oleksii Beizerov
 *
 */
public class ApplicationException extends Exception {

	private static final long serialVersionUID = 5987448876395444010L;

	
	public ApplicationException() {
		super();
	}
	
	public ApplicationException(String message) {
		super(message);
	}
	
	public ApplicationException(String message, Exception cause) {
		super(message, cause);
	}
}
