package ua.nure.beizerov.facultative.exception;


/**
 * An exception that provides information on an authentication error.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class AuthenticationException extends ApplicationException {

	private static final long serialVersionUID = 6645235106615827390L;

	
	public AuthenticationException() {
		super();
	}

	public AuthenticationException(String message) {
		super(message);
	}

	public AuthenticationException(String message, Exception cause) {
		super(message, cause);
	}
}
