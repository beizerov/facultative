package ua.nure.beizerov.facultative.exception.constant;


/**
 * @author Oleksii Beizerov
 *
 */
public final class ExceptionMessage {

	
	private ExceptionMessage() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	
	// For authentication
	public static final String
	AUTHENTICATION_FAILED = "Authentication failed";
}
