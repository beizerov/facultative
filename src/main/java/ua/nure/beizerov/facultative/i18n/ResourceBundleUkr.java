package ua.nure.beizerov.facultative.i18n;


import java.util.ListResourceBundle;


/**
 * This class represent a resource for the i18n of Ukrainian.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class ResourceBundleUkr extends ListResourceBundle {

	private static final Object[][] contents = {
			{"flag", "image/i18n/ukraine-flag-icon.png"},
			{"login_btn", "ВХІД"},
			{"registration_btn", "Реєстрація"}
	};
	
	
	@Override
	protected Object[][] getContents() {		
		return contents;
	}
}
