package ua.nure.beizerov.facultative.i18n;


import java.util.ListResourceBundle;


/**
 * This class represent a resource for the i18n of English UK.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class ResourceBundleEngGbr extends ListResourceBundle {

	private static final Object[][] contents = {
			{"flag", "image/i18n/united-kingdom-great-britain-flag-icon.png"},
			{"login_btn", "LOG IN"},
			{"registration_btn", "Registration"}
	};
	
	
	@Override
	protected Object[][] getContents() {
		return contents;
	}
}
