package ua.nure.beizerov.facultative.i18n;


import java.util.ListResourceBundle;


/**
 * This class represent a resource for the i18n of Russian.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class ResourceBundleRus extends ListResourceBundle {

	private static final Object[][] contents = {
			{"flag", "image/i18n/russia-flag-icon.png"},
			{"login_btn", "ВХОД"},
			{"registration_btn", "Регистраця"}
	};
	
	
	@Override
	protected Object[][] getContents() {
		return contents;
	}
}
