package ua.nure.beizerov.facultative.model;


import ua.nure.beizerov.facultative.exception.GradeException;


/**
 * This enumeration represents all valid grade.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public enum Grade {
	A_PLUS("A+", "97%-100%"), A("A", "93%-96%"), A_MINUS("A-", "90%-92%"),
	B_PLUS("B+", "87%-89%"), B("B", "83%-86%"), B_MINUS("B-", "80%-82%"),
	C_PLUS("C+", "77%-79%"), C("C", "73%-76%"), C_MINUS("C-", "70%-72%"),
	D_PLUS("D+", "67%-69%"), D("D", "63%-66%"), D_MINUS("D", "60%-62%"),
	F("F", "0%-59%");
	
	private final String letterGrade;
	private final String inPercentages;
	
	
	Grade(String letterGrade, String inPercentages) {
		this.letterGrade = letterGrade;
		this.inPercentages = inPercentages;
	}
	
	
	public String getLetterGrade() {
		return letterGrade;
	}
	
	public String getInPercentages() {
		return inPercentages;
	}
	
	public static Grade convertLetterGradeToGrade(String letterGrade)
			throws GradeException {
		for (Grade grade : Grade.values()) {
			if (grade.letterGrade.equals(letterGrade)) {
				return grade;
			}
		}
		
		throw new GradeException("Invalid format for grade");
	}
}
