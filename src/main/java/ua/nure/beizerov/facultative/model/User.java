package ua.nure.beizerov.facultative.model;


import java.util.Objects;


/**
 * The class represents the user model.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class User extends Entity {

	private static final long serialVersionUID = -1528252451860559220L;
	
	private Integer active;
	private String email;
	private String login;
	private String firstName;
	private String lastName;
	private String password;
	private Role role;
	private String locale;
	private String pathToPhoto;
	
	
	public Boolean isUnlocked() {
		return active != 0;
	}
	
	public Integer getActive() {
		return active;
	}
	
	public void setActive(Integer active) {
		this.active = active;
	}	

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getPathToPhoto() {
		return pathToPhoto;
	}

	public void setPathToPhoto(String pathToPhoto) {
		this.pathToPhoto = pathToPhoto;
	}
	

	@Override
	public int hashCode() {
		return Objects.hash(email, login);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		User other = (User) obj;
		
		return Objects.equals(email, other.email) 
				&& Objects.equals(login, other.login);
	}
}
