package ua.nure.beizerov.facultative.model;


import java.time.LocalDate;
import java.util.Objects;


/**
 * The class represents the course entity.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class Course extends Entity {

	private static final long serialVersionUID = 9144734809868085539L;

	private String name;
	private LocalDate startDate;
	private LocalDate endDate;
	private String description;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public LocalDate getStartDate() {
		return startDate;
	}
	
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	
	public LocalDate getEndDate() {
		return endDate;
	}
	
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		Course other = (Course) obj;
		
		return Objects.equals(name, other.name);
	}
}
