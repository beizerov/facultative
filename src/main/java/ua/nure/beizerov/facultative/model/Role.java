package ua.nure.beizerov.facultative.model;


/**
 * This enumeration represents all valid roles in the application.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public enum Role {
	ADMIN,
	TEACHER,
	STUDENT
}
