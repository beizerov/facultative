package ua.nure.beizerov.facultative.model;


import ua.nure.beizerov.facultative.persistence.Data;


/**
 * Entity is an abstract superclass for all entities.
 * The entities is used to represent the relationships of a domain model.
 * 
 * @author Oleksii Beizerov
 *
 */
public abstract class Entity implements Data {

	private static final long serialVersionUID = -2846853772479129596L;

	private long id;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
