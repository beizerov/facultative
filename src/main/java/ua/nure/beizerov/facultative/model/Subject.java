package ua.nure.beizerov.facultative.model;

import java.util.Objects;

/**
 * The class represents the subject entity.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class Subject extends Entity {

	private static final long serialVersionUID = 6332687694923768391L;

	private String name;
	private String description;

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		Subject other = (Subject) obj;
		
		return Objects.equals(name, other.name);
	}
}
